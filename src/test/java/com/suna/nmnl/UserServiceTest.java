package com.suna.nmnl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.suna.nmnl.model.User;
import com.suna.nmnl.service.UserService;

public class UserServiceTest {

	@Mock
	private UserService userService;
	private User user;
	
	@Before
	public void setUp() {
		user = new User();
		user.setFirstName("PETRO");
		user.setLastName("Ryndych");
		user.setLogin("pero");
		user.setId(1L);
		
		MockitoAnnotations.initMocks(this);
		Mockito.when(userService.save(user)).thenReturn(user);
		Mockito.when(userService.findByLogin(user.getLogin())).thenReturn(user);
		Mockito.when(userService.findByEmail(user.getEmail())).thenReturn(user);
		
	}
	
	@Test
	public void save_test(){
		Assert.assertEquals(user, userService.save(user));
	}
	
	@Test
	public void findByLogin_test(){
		Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
	}
	
	@Test
	public void findByEmail_test(){
		Assert.assertEquals(user, userService.findByEmail(user.getEmail()));
	}
}

