<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title><spring:message code="loginpage.title" /></title>
		
		<link href="/resources/css/materialize.min.css" rel="stylesheet">
		<link href="/resources/css/main.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	
	<body>
		<c:if test="${param.registration == 'success'}">
			<h3>
				<spring:message code="${param.message}" />
			</h3>
		</c:if>
		<c:if test="${param.registration == 'failed'}">
			<h3>
				<spring:message code="${param.message}" />
			</h3>
		</c:if>
		
		<div class="row">
			<form class="col s12 m6 l4 offset-m3 offset-l4" role="form"	action="/login" method="post">
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">account_circle</i>
					<input id="icon_prefix" type="text" class="validate" name="username" />
					<label for="icon_prefix">
						<spring:message code="loginpage.login" />
					</label>
				</div>
	
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">vpn_key</i>
					<input id="password" type="password" class="validate" name="password" />
					<label for="password">
						<spring:message code="loginpage.password" />
					</label>
				</div>
	
				<button class="btn-large waves-effect waves-light " type="submit"
					style="float: right; margin: 10px;" name="action">
					<spring:message code="loginpage.submit" />
				</button>
			</form>
		</div>
	
		<!-- Mainly scripts -->
		<script src="/resources/js/jquery-2.1.1.min.js"></script>
		<script src="/resources/js/jquery.validate-1.14.0.min.js"></script>
		<script src="/resources/js/materialize.min.js"></script>
	
	</body>
</html>




