<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title><spring:message code="registrationpage.title" /></title>
		
		<link href="/resources/css/materialize.min.css" rel="stylesheet">
		<link href="/resources/css/main.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
	</head>

	<body>
		<div class="row">
			<form:form commandName="user" method="post"
				cssClass="col s12 m6 l4 offset-m3 offset-l4 registration-form">
				
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">account_circle</i>
					<form:input id="login" path="login" />
					<form:errors path="login" cssClass="red-text text-darken-4" />
					<label for="login">
						<spring:message code="registrationpage.login" />
					</label>
				</div>
	
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">email</i>
					<form:input id="email" path="email" />
					<form:errors path="email" cssClass="red-text text-darken-4" />
					<label for="email" data-error="wrong" data-success="right">
						<spring:message	code="registrationpage.email" />
					</label>
				</div>
	
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">vpn_key</i>
					<form:password id="password" path="password" />
					<form:errors path="password" cssClass="red-text text-darken-4" />
					<label for="password">
						<spring:message code="registrationpage.password" />
					</label>
				</div>
	
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">vpn_key</i>
					<input type="password" name="repassword" id="repassword">
					<label for="repassword">
						<spring:message	code="registrationpage.password.repeat" />
					</label>
				</div>
	
				<button class="btn-large waves-effect waves-light " type="submit"
					style="float: right; margin: 10px;" name="action">
					<spring:message code="registrationpage.submit" />
				</button>
			</form:form>
		</div>
	
		<fb:login-button scope="public_profile,email"
			onlogin="checkLoginState();">
		</fb:login-button>
	
		<div id="status"></div>
		
		<!-- Mainly scripts -->
		<script src="/resources/js/jquery-2.1.1.min.js"></script>
		<script src="/resources/js/jquery.validate-1.14.0.min.js"></script>
		<script src="/resources/js/materialize.min.js"></script>
		<script src="/resources/js/facebook.js"></script>
		
		<script>
			$(document).ready(function() {
				$(".registration-form").validate({
					errorElement : 'span',
					errorClass : 'error red-text text-darken-4 invalid',
					validClass : "valid",
					rules : {
						login : {
							required : true,
							minlength : 3,
							remote : {
								url : "/registration/available/login",
								type : "post",
								data : {
									login : function() {
										return $("#login").val();
									}
								}
							}
						},
						email : {
							required : true,
							email : true,
							minlength : 3,
							remote : {
								url : "/registration/available/email",
								type : "post",
								data : {
									login : function() {
										return $("#email").val();
									}
								}
							}
						},
						password : {
							required : true,
							minlength : 6
						},
						repassword : {
							required : true,
							minlength : 6,
							equalTo : "#password"
						}
					},
					messages : {
						login : {
							remote : "<spring:message code="registrationpage.error.login" />"
						},
						email : {
							remote : "<spring:message code="registrationpage.error.email" />"
						}
					}
				});
			});
		</script>
	</body>
</html>
