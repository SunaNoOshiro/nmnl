<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title><spring:message code="homepage.title"/></title>
		
		<link href="/resources/css/materialize.min.css" rel="stylesheet">
		<link href="/resources/css/main.css" rel="stylesheet">
		<link href="/resources/css/angular-material.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
			rel="stylesheet">
		
		<!-- Mainly scripts -->
		<script src="/resources/js/jquery-2.1.1.min.js"></script>
		<script src="/resources/js/jquery.validate-1.14.0.min.js"></script>
		<script src="/resources/js/soundcloud-sdk-2.0.0.js"></script>
		
		<script src="/resources/js/angular.min.js"></script>
		<script src="/resources/js/materialize.min.js"></script>
		
		<script>SC.initialize({client_id : 'd57e1fb0affc7dc39db62724b0f86fc8'});</script>	
		<script src="/resources/app/app.js"></script>
		<script src="/resources/js/customParamAjax.js"></script>		
	</head>
	
	<body ng-app="nmnl">
		<div class="row">
			<player-div></player-div>
		</div>
	
		<div class="row">
			<tabs class="col s6 offset-s3" style=" position: fixed;height:100%"></tabs>
		</div>
		
<!-- 		<script src="/resources/js/soundcloud.js"></script> -->
	</body>
</html>
