
	'use strict';
	
	var app = angular.module("nmnl", []);

	app.controller('playedSongsController', [ '$http', function($http) {
		var nmnl = this;
		nmnl.songs = [];
		nmnl.listened= [];
		nmnl.songsToShow = [];
		nmnl.id = 0;
		$http.get('/api/songs').success(function(data) {
			nmnl.songs = data;
			nmnl.listened = data;
			for(var app = 0; i < nmnl.listened.length; i++){
				if(nmnl.listened[i].artworkUrl == ''){
					nmnl.listened[i].artworkUrl = 'http://ardennehigh.com/templates/default/images/music_notes.gif';
				}
			}
		});

		this.next = function() {
			if(nmnl.id < nmnl.listened.length){
				if(nmnl.songsToShow.length >= 4){				
					nmnl.songsToShow.shift();
					nmnl.songsToShow.push(nmnl.listened[nmnl.id]);
					nmnl.id ++;				
				}
				else{				
					nmnl.songsToShow.push(nmnl.listened[nmnl.id]);
					nmnl.id ++;				
				}
			}
			
		}
		this.prev = function() {
			if(nmnl.songsToShow.length >= 4 && nmnl.id > 4){
				nmnl.songsToShow.pop();
				nmnl.songsToShow.unshift(nmnl.listened[nmnl.id - 5]);
				nmnl.id--;
			}
		}
	} ]);

	
	
	 app.directive("createStation", function() {
	    return {
	      restrict: 'E',
	      templateUrl: "/resources/app/part/create-station.html",
	      controller: function($http, $scope) {
	  		var newStation = this;

			newStation.query = '';
			newStation.results = {songs:[], artists:[], albums:[], genres:[]};
			newStation.clear = function(){
				newStation.query = '';				
				newStation.results = {songs:[], artists:[], albums:[], genres:[]};				
			};			
			
			newStation.search = function(){
				if(newStation.query == ''){
					newStation.clear();
				}
				else{					
					var getResults = function(type){
						var url = '/api/' + type + '/search/';						
						$http.get(url+ newStation.query).success(function(data) {
							newStation.results[type] = data;
						}).error(function(data) {
							newStation.results[type] = [];
						});	
					};					
					getResults('songs');
					getResults('artists');
					getResults('albums');
					getResults('genres');
					
				}			
			};
			
			newStation.create = function(item, type){
				
				var type_id  =  type + "_"+ item.id;
				var type_rates = type + "Rates";
				var station = {};
				station.name = item.name === undefined ? station.name = item.title: station.name = item.name;
				station[type_rates] = {};
				station[type_rates][type_id] =  {
						"id" : 1
				};		
				
				$http.post('/api/profile/stations', station).success(function(data) {
					newStation.clear();
					$scope.refreshStations();
				});
			}

		},
		controllerAs:'ctrl'
	    };
	  });
	 
	 app.directive("tabs", function() {
	    return {
	      restrict: 'E',
	      templateUrl: "/resources/app/part/tabs.html",
	      controller: function($http) {
	    	  var tabs = this;
	    	  tabs.current = 1;
	    	  tabs.setTab = function(tab){
	    		  tabs.current = tab;
	    	  };
	    	  tabs.isSet = function(tab){
	    		  return tabs.current == tab;
	    	  };
	    	  
		},
		controllerAs:'tab'
	    };
	  });
	 
	 app.directive("nowPlaying", function() {
		    return {
		      restrict: 'E',
		      templateUrl: "/resources/app/part/now-playing.html",
		      controller: function($http, $scope) {
		    	  $scope.refreshStations = function(){
		    		  $http.get('/api/profile/stations/').success(function(data) {					
							$scope.stations = data;
						});
		    	  };
		    	  var play = this;
		    	  $scope.refreshStations();
			},
			controllerAs:'play'
		    };
		  });
	 
	 app.directive("profile", function() {
		    return {
		      restrict: 'E',
		      templateUrl: "/resources/app/part/profile.html",
		      controller: function($http) {
		    	  var profile = this;
		    	  
			},
			controllerAs:'profile'
		    };
		  });
	 
	 app.directive("discover", function() {
		    return {
		      restrict: 'E',
		      templateUrl: "/resources/app/part/discover.html",
		      controller: function($http) {
		    	  var discover = this;
		    	  
			},
			controllerAs:'discover'
		    };
		  });
	 
	 app.directive("playerDiv", function() {
		    return {
		      restrict: 'E',
		      templateUrl: "/resources/app/part/player-div.html",
		      controller: function($http) {
		    	  var player = this;
		    	  
			},
			controllerAs:'player'
		    };
		  });
	 
	 app.directive("player", function() {
		    return {
		      restrict: 'E',
		      templateUrl: "/resources/app/part/player.html",
		      controller: function($http, $scope) {
		    	  var player = this;
		    	  player.volume = 100;
		    	  player.muted = false;
		    	  player.volumeIconName = 'volume_up';
		    	  player.volumeUnmutedIcon;
		    	  player.volumeMutedIcon = 'volume_off';
		    	  player.played = true;
		    	  $scope.liked = false;
		    	  $scope.unliked = false;
		    	  $scope.playedIcon = 'pause'; 
		    	  player.duration = 0;
		    	  player.currentPossition = 0;
		    	  
		    	  player.increment = function() {
		    		  if( player.played == true){
		    			  if(player.duration<=0){
		    				  player.next();
		    			  }
		    			  player.duration = player.duration-1000 ;
				    	  player.currentPossition = player.currentPossition+1000;
				    	  $("#hiddenUpdater").click();
		    		  }
		    		     setTimeout(player.increment,1000);
		    		};
		    	  
		    	  player.msToTime = function(s, invert) {
		    		  var ms = s % 1000;
		    		  s = (s - ms) / 1000;
		    		  var secs = s % 60;
		    		  s = (s - secs) / 60;
		    		  var mins = s % 60;
		    		  if(invert==true){
		    			  return  "-" + mins + ':' + secs ;
		    		  }
		    		  else
		    			  return  mins + ':' + secs ;
		    		}

		    	  player.init = function(){
		    		  $http.get("/api/recommendations/station/1").success(function(data) {
		    			  $scope.song = data;
		    			  if($scope.song.artworkUrl === undefined || $scope.song.artworkUrl == null ||  $scope.song.artworkUrl ==''){
		    				  $scope.song.artworkUrl = 'http://ardennehigh.com/templates/default/images/music_notes.gif';
							}
		    			  SC.stream("/tracks/"+data.soundcloudId,function(sound){
			    			  player.stream = sound;	
			    			  player.stream.play();
			    			  player.duration = player.stream.getDuration();
			    			  player.currentPossition = player.stream.getCurrentPosition();
			    			  player.increment();
			    			  });
						}).error(function(data) {
							console.log("error");
						});	
		    		  
		    		
		    		  
		    		
		    	  };
		    	  
		    	  player.stream = player.init();
		    	  
		    	  player.playTrack = function() {
		    	     player.stream.play();
		    	     
		    	  };
		    	  
		    	  player.pauseTrack = function() {
		    	    if(player.stream) {
		    	    	player.stream.pause();
		    	    }
		    	  };		    	  
		    	  
		    	  player.changeVolume = function(){
		    		  if( player.volume > 50 ){
		    			  player.volumeIconName = 'volume_up';
		    		  }
		    		  if( player.volume > 0 && player.volume < 50 ){
		    			  player.volumeIconName = 'volume_down';
		    		  }
		    		  if( player.volume == 0){
		    			  player.volumeIconName = 'volume_mute';
		    		  }

		    		  if( player.muted == true){
		    			  player.muted = false;
		    			  toast('unmuted', 1000);
		    		  }
		    		  player.stream.setVolume(player.volume/100);
		    	  };
		    	  
		    	  player.toggleMute = function(){
		    		  if( player.muted == true){
		    			  player.muted = false;
		    			  player.volumeIconName = player.volumeUnmutedIcon;	
		    			  toast('unmuted', 1000);
		    			  player.stream.setVolume(player.volume/100);
		    		  }
		    		  else {
		    			  player.muted = true;
		    			  player.volumeUnmutedIcon =  player.volumeIconName;
		    			  player.volumeIconName = player.volumeMutedIcon;
		    			  player.stream.setVolume(0);
		    			  toast('muted', 1000);
		    		  }
		    	  };
		    	  
			      player.playOrStop = function(){
			    	  if( player.played == true){
			    		  $scope.playedIcon = 'play_arrow';
			    		  player.played = false;
			    		  toast('paused', 1000);
			    		  player.pauseTrack();
			    	  }
			    	  else{
			    		  $scope.playedIcon = 'pause';			    		  
			    		  player.played = true;
			    		  toast('played', 1000);
			    		  player.playTrack();
			    	  }
			      };
			      
			      player.likeUnlike = function(){
			    	  if($scope.liked == false){
			    		  $scope.liked = true;
			    		  $scope.unliked = false;	
			    		  toast('like', 1000);
			    	  }
			    	  else{
			    		  $scope.liked = false;	
			    			toast('unlike', 1000);
			    	  }
			      };
			      
			      player.dislikeUndislike = function(){
			    	  if($scope.unliked == false){
			    		  $scope.unliked = true;
			    		  $scope.liked = false;
		    			  toast('dislike', 1000);
			    	  }
			    	  else{
			    		  $scope.unliked = false;
			    		  toast('undislike', 1000);
			    	  }
			      };
			      
			      player.next = function(){
			    	  toast('next song', 1000);
			    	  player.stream.stop();
			    	  $http.get("/api/recommendations/station/1").success(function(data) {
			    		  $scope.song = data;
		    			  if($scope.song.artworkUrl === undefined || $scope.song.artworkUrl == null ||  $scope.song.artworkUrl ==''){
		    				  $scope.song.artworkUrl = 'http://ardennehigh.com/templates/default/images/music_notes.gif';
							}
		    			  SC.stream("/tracks/"+data.soundcloudId,function(sound){
			    			  player.stream = sound;	
			    			  $scope.playedIcon = 'pause';	
			    			  $scope.liked= true;
			    			  $scope.unliked= false;
				    		  player.played = true; 
				    		  player.playTrack();
			    			  player.duration = player.stream.getDuration();
			    			  player.currentPossition = player.stream.getCurrentPosition();
			    			  player.stream.setVolume(player.volume/100);
			    			 
			    			 
			    			  });
						}).error(function(data) {
							console.log("error");
						});	
			      }
			      
		    	  
			},
			controllerAs:'player'
		    };
		  });
	 
	 app.directive("songInfo", function() {
		    return {
		      restrict: 'E',
		      templateUrl: "/resources/app/part/song-info.html",
		      controller: function($http, $scope) {
		    	  
		      }
		    ,
			controllerAs:'info'
		    }
	 });