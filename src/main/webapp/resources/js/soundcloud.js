var page_size = 200;

for (var i = 0; i < 1; i++) {
	// console.log(i+1);
	SC
			.get(
					'/tracks',
					{
						limit : page_size,
						linked_partitioning : i + 4
					},
					function(tracks) {

						for (var index = 0; index < tracks.collection.length; index++) {
							var song = {};
							var genre = {
								"id" : 1
							};

							song.title = tracks.collection[index].title;
							song.soundcloudId = tracks.collection[index].id;
							song.streamUrl = tracks.collection[index].stream_url == null ? ''
									: tracks.collection[index].stream_url;
							song.downloadUrl = tracks.collection[index].download_url == null ? ''
									: tracks.collection[index].download_url;
							song.uri = tracks.collection[index].uri == null ? ''
									: tracks.collection[index].uri;
							song.genres = [ genre ];
							song.bpm = tracks.collection[index].bpm == null ? 0
									: tracks.collection[index].bpm;
							song.duration = tracks.collection[index].duration == null ? 0
									: tracks.collection[index].duration;
							song.description = tracks.collection[index].description == null ? ''
									: tracks.collection[index].description;
							song.artworkUrl = tracks.collection[index].artwork_url == null ? ''
									: tracks.collection[index].artwork_url;

							$.post("/api/songs", $.customParam(song), function(
									data) {
								console.log('done')
							}, "json");

							$.ajax({
								url : "/api/songs",
								method : "post",
								data : song
							}).done(function(data) {
								console.log('done')
							})

						}
						console
								.log('----------------------------------------------------------');
					});
}