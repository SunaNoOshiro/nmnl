package com.suna.nmnl.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.suna.nmnl.annotation.UniqueSoundCloudId;
import com.suna.nmnl.repository.SongRepository;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class UniqueSoundCloudIdValidator implements ConstraintValidator<UniqueSoundCloudId, Long> {

	@Resource(name = Repositories.SONG_REPOSITORY)
	private SongRepository songRepository;

	@Override
	public void initialize(UniqueSoundCloudId constraintAnnotation) {

	}

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (songRepository == null)
			return true;
		return songRepository.findBySoundcloudId(value) == null;
	}

}
