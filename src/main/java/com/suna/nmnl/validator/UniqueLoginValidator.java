package com.suna.nmnl.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.suna.nmnl.annotation.UniqueLogin;
import com.suna.nmnl.repository.UserRepository;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class UniqueLoginValidator implements ConstraintValidator<UniqueLogin, String> {

	@Resource(name = Repositories.USER_REPOSITORY)
	private UserRepository userRepository;

	@Override
	public void initialize(UniqueLogin constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (userRepository == null)
			return true;
		return userRepository.findByLogin(value) == null;
	}

}
