package com.suna.nmnl.populator;

import java.util.ArrayList;
import java.util.List;

import com.suna.nmnl.model.AbstractModel;

public abstract class AbstractPopulator<SOURCE, TARGET> {

	public abstract void populate(SOURCE source, TARGET target);

	protected List<Integer> getListOfIds(List<? extends AbstractModel> models) {
		List<Integer> listOfIds = null;

		if (models != null && !models.isEmpty()) {
			listOfIds = new ArrayList<>();
			
			for (AbstractModel model : models) {
				listOfIds.add(model.getId().intValue());
			}
		}

		return listOfIds;

	}
}
