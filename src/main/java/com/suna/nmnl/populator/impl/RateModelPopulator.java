package com.suna.nmnl.populator.impl;

import com.suna.nmnl.enums.RateEnum;
import com.suna.nmnl.model.Rate;
import com.suna.nmnl.model.dto.RateDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class RateModelPopulator extends AbstractPopulator<RateDTO, Rate> {

	@Override
	public void populate(RateDTO data, Rate model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setName(getName(data));
		}
	}

	private RateEnum getName(RateDTO data) {
		String name = data.getName();
		return name == null ? null : RateEnum.valueOf(name);
	}

}
