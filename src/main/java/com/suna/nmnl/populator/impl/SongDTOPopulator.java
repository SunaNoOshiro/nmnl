package com.suna.nmnl.populator.impl;

import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.SongDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class SongDTOPopulator extends AbstractPopulator<Song, SongDTO> {

	@Override
	public void populate(Song model, SongDTO data) {
		if (data != null && model != null) {
			data.setSoundcloudId(model.getSoundcloudId());
			data.setTitle(model.getTitle());
			data.setDescription(model.getDescription());
			data.setDuration(model.getDuration());
			data.setBpm(model.getBpm());
			data.setUri(model.getUri());
			data.setArtworkUrl(model.getArtworkUrl());
			data.setDownloadUrl(model.getDownloadUrl());
			data.setStreamUrl(model.getStreamUrl());
			data.setAlbum(getAlbum(model));
			data.setArtist(getArtist(model));
			data.setGenres(getListOfIds(model.getGenres()));
		}
	}

	private Long getArtist(Song model) {
		Artist artist = model.getArtist();
		return artist == null ? null : artist.getId();
	}

	private Long getAlbum(Song model) {
		Album album = model.getAlbum();
		return album == null ? null : album.getId();
	}

}
