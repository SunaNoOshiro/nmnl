package com.suna.nmnl.populator.impl;

import com.suna.nmnl.model.PlayingType;
import com.suna.nmnl.model.dto.PlayingTypeDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class PlayingTypeDTOPopulator extends AbstractPopulator<PlayingType, PlayingTypeDTO> {

	@Override
	public void populate(PlayingType model, PlayingTypeDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setName(model.getName());
		}
	}
}
