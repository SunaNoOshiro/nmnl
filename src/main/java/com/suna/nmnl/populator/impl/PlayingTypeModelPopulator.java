package com.suna.nmnl.populator.impl;

import com.suna.nmnl.enums.PlayEnum;
import com.suna.nmnl.model.PlayingType;
import com.suna.nmnl.model.dto.PlayingTypeDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class PlayingTypeModelPopulator extends AbstractPopulator<PlayingTypeDTO, PlayingType> {

	@Override
	public void populate(PlayingTypeDTO data, PlayingType model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setName(getName(data));
		}
	}

	private PlayEnum getName(PlayingTypeDTO data) {
		String name = data.getName();
		return name == null ? null : PlayEnum.valueOf(name);
	}
}
