package com.suna.nmnl.populator.impl;

import com.suna.nmnl.model.Station;
import com.suna.nmnl.model.dto.StationDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class StationDTOPopulator extends AbstractPopulator<Station, StationDTO> {

	@Override
	public void populate(Station model, StationDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setName(model.getName());
			data.setUser(model.getId());
		}
	}
}
