package com.suna.nmnl.populator.impl;

import java.util.Date;

import com.suna.nmnl.enums.Gender;
import com.suna.nmnl.model.User;
import com.suna.nmnl.model.dto.UserDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class UserDTOPopulator extends AbstractPopulator<User, UserDTO> {

	@Override
	public void populate(User model, UserDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setEmail(model.getEmail());
			data.setBirthday(getBirthday(model));
			data.setCountryCode(model.getCountryCode());
			data.setFirstName(model.getFirstName());
			data.setLastName(model.getLastName());
			data.setGender(getGender(model));
			data.setLanguage(model.getLanguage());
			data.setStations(getListOfIds(model.getStations()));
			data.setActive(model.isActive());
			data.setLogin(model.getLogin());
		}
	}

	private String getGender(User model) {
		Gender gender = model.getGender();
		return gender == null ? null : gender.getValue();
	}

	private Long getBirthday(User model) {
		Date birthday = model.getBirthday();
		return birthday == null ? null : birthday.getTime();
	}

}
