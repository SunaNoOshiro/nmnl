package com.suna.nmnl.populator.impl;

import java.util.Date;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.dto.AlbumDTO;
import com.suna.nmnl.model.dto.ArtistDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;

public class AlbumDTOPopulator extends AbstractPopulator<Album, AlbumDTO> {

	@Resource(name = Converters.ARTIST_MODEL_CONVERTER)
	private AbstractConverter<Artist, ArtistDTO> artistConverter;

	@Override
	public void populate(Album model, AlbumDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setName(model.getName());
			data.setReleaseDate(getReleaseDate(model));
			data.setDescription(model.getDescription());
			data.setSongs(getListOfIds(model.getSongs()));
			data.setArtist(getArtist(model));
		}
	}

	private Long getReleaseDate(Album model) {
		Date releaseDate = model.getReleaseDate();
		return releaseDate == null ? null : releaseDate.getTime();
	}

	private ArtistDTO getArtist(Album model) {
		return artistConverter.convert(model.getArtist());
	}
}
