package com.suna.nmnl.populator.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.Genre;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.SongDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.service.AlbumService;
import com.suna.nmnl.service.ArtistService;
import com.suna.nmnl.service.GenreService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class SongModelPopulator extends AbstractPopulator<SongDTO, Song> {

	@Resource(name = Services.ALBUM_SERVICE)
	private AlbumService albumService;

	@Resource(name = Services.GENRE_SERVICE)
	private GenreService genreService;

	@Resource(name = Services.ARTIST_SERVICE)
	private ArtistService artistService;

	@Override
	public void populate(SongDTO data, Song model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setSoundcloudId(data.getSoundcloudId());
			model.setTitle(data.getTitle());
			model.setDescription(data.getDescription());
			model.setDuration(data.getDuration());
			model.setBpm(data.getBpm());
			model.setUri(data.getUri());
			model.setArtworkUrl(data.getArtworkUrl());
			model.setDownloadUrl(data.getDownloadUrl());
			model.setStreamUrl(data.getStreamUrl());
			model.setAlbum(getAlbum(data));
			model.setArtist(getArtist(data));
			model.setGenres(getGenres(data));
		}
	}

	private List<Genre> getGenres(SongDTO data) {
		List<Genre> genres = new ArrayList<>();

		if (data.getGenres() != null) {
			for (Integer id : data.getGenres()) {
				genres.add(genreService.findOne(id.longValue()));
			}
		}

		return genres;
	}

	private Artist getArtist(SongDTO data) {
		Long artist = data.getArtist();
		return artist == null ? null : artistService.findOne(artist);
	}

	private Album getAlbum(SongDTO data) {
		Long album = data.getAlbum();
		return album == null ? null : albumService.findOne(album);
	}
}
