package com.suna.nmnl.populator.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Genre;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.GenreDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.service.SongService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class GenreModelPopulator extends AbstractPopulator<GenreDTO, Genre> {

	@Resource(name = Services.SONG_SERVICE)
	private SongService songService;

	@Override
	public void populate(GenreDTO data, Genre model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setName(data.getName());
			model.setSongs(getSongs(data));
		}
	}

	private List<Song> getSongs(GenreDTO data) {
		Long id = data.getId();
		return id == null ? null : songService.findSongsByGenre(id);
	}
}
