package com.suna.nmnl.populator.impl;

import javax.annotation.Resource;

import com.suna.nmnl.model.Station;
import com.suna.nmnl.model.User;
import com.suna.nmnl.model.dto.StationDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.service.UserService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class StationModelPopulator extends AbstractPopulator<StationDTO, Station> {

	@Resource(name = Services.USER_SERVICE)
	private UserService userService;

	@Override
	public void populate(StationDTO data, Station model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setName(data.getName());
			model.setUser(getUser(data));
		}
	}

	private User getUser(StationDTO data) {
		Long id = data.getId();
		return id == null ? null : userService.findOne(id);
	}
}
