package com.suna.nmnl.populator.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.AlbumDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.service.ArtistService;
import com.suna.nmnl.service.SongService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class AlbumModelPopulator extends AbstractPopulator<AlbumDTO, Album> {

	@Resource(name = Services.SONG_SERVICE)
	private SongService songService;

	@Resource(name = Services.ARTIST_SERVICE)
	private ArtistService artistService;

	@Override
	public void populate(AlbumDTO data, Album model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setName(data.getName());
			model.setDescription(data.getDescription());
			model.setReleaseDate(getReleaseDate(data));
			model.setArtist(getArtist(data));
			model.setSongs(getSongs(data));
		}
	}

	private Date getReleaseDate(AlbumDTO data) {
		Long releaseDate = data.getReleaseDate();
		return releaseDate == null ? null : new Date(releaseDate);
	}

	private Artist getArtist(AlbumDTO data) {
		Long id = data.getId();
		return id == null ? null : artistService.findOne(id);
	}

	private List<Song> getSongs(AlbumDTO data) {
		Long id = data.getId();
		return id == null ? null : songService.findSongsByAlbum(id);
	}
}
