package com.suna.nmnl.populator.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.dto.ArtistDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.service.AlbumService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class ArtistModelPopulator extends AbstractPopulator<ArtistDTO, Artist> {

	@Resource(name = Services.ALBUM_SERVICE)
	private AlbumService albumService;

	@Override
	public void populate(ArtistDTO data, Artist model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setName(data.getName());
			model.setDescription(data.getDescription());
			model.setAlbums(getAlbums(data));
		}
	}

	private List<Album> getAlbums(ArtistDTO data) {
		Long id = data.getId();
		return id == null ? null : albumService.findAlbumsByArtistId(id);
	}
}
