package com.suna.nmnl.populator.impl;

import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.dto.ArtistDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class ArtistDTOPopulator extends AbstractPopulator<Artist, ArtistDTO> {

	@Override
	public void populate(Artist model, ArtistDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setName(model.getName());
			data.setDescription(model.getDescription());
			data.setAlbums(getListOfIds(model.getAlbums()));
		}
	}

}
