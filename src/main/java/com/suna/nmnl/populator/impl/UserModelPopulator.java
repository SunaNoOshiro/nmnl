package com.suna.nmnl.populator.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.enums.Gender;
import com.suna.nmnl.model.Station;
import com.suna.nmnl.model.User;
import com.suna.nmnl.model.dto.UserDTO;
import com.suna.nmnl.populator.AbstractPopulator;
import com.suna.nmnl.service.StationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class UserModelPopulator extends AbstractPopulator<UserDTO, User> {

	@Resource(name = Services.STATION_SERVICE)
	private StationService stationService;

	@Override
	public void populate(UserDTO data, User model) {
		if (data != null && model != null) {
			model.setId(data.getId());
			model.setEmail(data.getEmail());
			model.setBirthday(getBirthday(data));
			model.setCountryCode(data.getCountryCode());
			model.setFirstName(data.getFirstName());
			model.setLastName(data.getLastName());
			model.setGender(getGender(data));
			model.setLanguage(data.getLanguage());
			model.setStations(getStations(data));
			model.setActive(data.isActive());
			model.setLogin(data.getLogin());
		}
	}

	private Gender getGender(UserDTO data) {
		String gender = data.getGender();
		return gender == null ? null : Gender.valueOf(gender);
	}

	private Date getBirthday(UserDTO data) {
		Long birthday = data.getBirthday();
		return birthday == null ? null : new Date(birthday);
	}

	private List<Station> getStations(UserDTO data) {
		Long id = data.getId();
		return id == null ? null : stationService.findByUserId(id);
	}
}
