package com.suna.nmnl.populator.impl;

import com.suna.nmnl.model.Genre;
import com.suna.nmnl.model.dto.GenreDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class GenreDTOPopulator extends AbstractPopulator<Genre, GenreDTO> {

	@Override
	public void populate(Genre model, GenreDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setName(model.getName());
			data.setSongs(getListOfIds(model.getSongs()));
		}
	}

}
