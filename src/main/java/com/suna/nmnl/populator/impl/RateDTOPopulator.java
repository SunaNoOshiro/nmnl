package com.suna.nmnl.populator.impl;

import com.suna.nmnl.model.Rate;
import com.suna.nmnl.model.dto.RateDTO;
import com.suna.nmnl.populator.AbstractPopulator;

public class RateDTOPopulator extends AbstractPopulator<Rate, RateDTO> {

	@Override
	public void populate(Rate model, RateDTO data) {
		if (data != null && model != null) {
			data.setId(model.getId());
			data.setName(model.getName());
		}
	}
}
