package com.suna.nmnl.enums;

public enum RateEnum {
	LIKED("liked"),
	DISLIKED("disliked");

	private final String value;

	RateEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
