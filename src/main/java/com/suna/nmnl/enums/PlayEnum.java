package com.suna.nmnl.enums;

public enum PlayEnum {
	TODAY("today"),
	NEVER("never");

	private final String value;

	PlayEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
