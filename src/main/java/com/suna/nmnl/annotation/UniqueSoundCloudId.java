package com.suna.nmnl.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.suna.nmnl.validator.UniqueSoundCloudIdValidator;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = {UniqueSoundCloudIdValidator.class})
public @interface UniqueSoundCloudId {
	String message();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
