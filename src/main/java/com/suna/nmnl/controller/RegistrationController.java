package com.suna.nmnl.controller;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import com.suna.nmnl.facade.MailConfirmationFacade;
import com.suna.nmnl.facade.UserFacade;
import com.suna.nmnl.util.ConstantUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.nmnl.model.User;
import com.suna.nmnl.util.ConstantUtil.Message.RegistrationMessage;
import com.suna.nmnl.util.ConstantUtil.Web.ModelAttributes;
import com.suna.nmnl.util.ConstantUtil.Web.Page;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Web.RequestParams;

@Controller
@RequestMapping(RequestMappingPath.REGISTRATION_BASE_URL)
public class RegistrationController {

    private static final String FAILED_REGISTRATION_STATUS = "failed";
    private static final String SUCCESS_REGISTRATION_STATUS = "success";

    @Resource(name = ConstantUtil.Beans.Facades.USER_FACADE)
    private UserFacade userFacade;

    @Resource(name = ConstantUtil.Beans.Facades.MAIL_CONFIRMATION_FACADE)
    private MailConfirmationFacade mailConfirmationFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String showView() {
        return Page.REGISTRATION_PAGE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String register(Model model, @Valid @ModelAttribute(ModelAttributes.USER_MODEL) User user,
                           BindingResult result) {

        String returnPage = Page.REDIRECT_TO_LOGIN_PAGE;

        if (!result.hasErrors()) {
            userFacade.save(user);
            mailConfirmationFacade.sendEmailAndSaveConfirmation(user);
            updateModelAttributes(model, SUCCESS_REGISTRATION_STATUS, RegistrationMessage.REGISTRATION_CHECK_MESSAGE);

        } else {
            returnPage = Page.REGISTRATION_PAGE;
        }

        return returnPage;
    }

    @RequestMapping(value = UrlPart.CONFIRM, method = RequestMethod.GET)
    @Transactional
    public String confirmRegistration(Model model, @RequestParam(RequestParams.TOKEN) String token) {
        User user = userFacade.findByToken(token);
        String redirectPage = Page.REDIRECT_TO_LOGIN_PAGE;

        if (user == null || user.isActive() || !mailConfirmationFacade.isValid(token)) {
            updateModelAttributes(model, FAILED_REGISTRATION_STATUS,
                    RegistrationMessage.REGISTRATION_UNCOMPLETED_MESSAGE);

        } else {
            activateUser(token, user);
            updateModelAttributes(model, SUCCESS_REGISTRATION_STATUS,
                    RegistrationMessage.REGISTRATION_COMPLETED_MESSAGE);
        }

        return redirectPage;
    }

    @RequestMapping(value = UrlPart.AVAILABLE_LOGIN, method = RequestMethod.POST)
    @ResponseBody
    public Boolean loginExist(@RequestParam(RequestParams.LOGIN) String login) {
        return userFacade.findByLogin(login) == null;
    }

    @RequestMapping(value = UrlPart.AVAILABLE_EMAIL, method = RequestMethod.POST)
    @ResponseBody
    public Boolean emailExist(@RequestParam(RequestParams.EMAIL) String email) {
        return userFacade.findByEmail(email) == null;
    }

    @ModelAttribute(ModelAttributes.USER_MODEL)
    public User user() {
        return new User();
    }

    private void activateUser(String token, User user) {
        user.setActive(true);
        userFacade.update(user);
        mailConfirmationFacade.delete(token);
    }

    private void updateModelAttributes(Model model, String status, String message) {
        model.addAttribute(ModelAttributes.REGISTRATION_STATUS, status);
        model.addAttribute(ModelAttributes.MESSAGE, message);
    }
}
