package com.suna.nmnl.controller.api;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import com.suna.nmnl.facade.SongFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.nmnl.model.Song;
import com.suna.nmnl.util.ConstantUtil.Web.ModelAttributes;
import com.suna.nmnl.util.ConstantUtil.Web.PathVariables;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Beans.Facades;

@Controller
@RequestMapping(RequestMappingPath.SONGS_API_BASE_URL)
public class SongController {

	@Resource(name = Facades.SONG_FACADE)
	private SongFacade songFacade;

	@RequestMapping
	@ResponseBody
	public List<Song> getSongs() {
		List<Song> songs = songFacade.findAll();
		List<Song> res = new ArrayList<>();
		for (Song song : songs) {
			res.add(songFacade.findSongWithGenres(song.getId()));
		}
		return res;
	}

	@RequestMapping(UrlPart.FIND_BY_ID)
	@ResponseBody
	public Song getSong(@PathVariable(PathVariables.ID) Long id) {
		return songFacade.findSongWithGenres(id);
	}

	@RequestMapping(UrlPart.FIND_BY_QUERY)
	@ResponseBody
	public List<Song> findSong(@PathVariable(PathVariables.QUERY) String query) {
		return songFacade.findSongsByQuery(query);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public Song addSong(@Valid @ModelAttribute(ModelAttributes.SONG_MODEL) Song song) {
		songFacade.save(song);
		return song;
	}

	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public Song updateSong(@ModelAttribute(ModelAttributes.SONG_MODEL) Song song) {
		if (song.getSoundcloudId() != null) {
			songFacade.save(song);
		}
		return song;
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public Song deleteSong(@ModelAttribute(ModelAttributes.SONG_MODEL) Song song) {
		if (song.getSoundcloudId() != null) {
			songFacade.delete(song);
		}
		return song;
	}

	@ModelAttribute(ModelAttributes.SONG_MODEL)
	public Song song() {
		return new Song();
	}
}
