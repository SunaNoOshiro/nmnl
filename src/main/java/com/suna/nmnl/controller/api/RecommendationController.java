package com.suna.nmnl.controller.api;

import javax.annotation.Resource;

import com.suna.nmnl.facade.RecommendationFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.nmnl.model.Song;
import com.suna.nmnl.util.ConstantUtil.Web.PathVariables;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Beans.Facades;

@Controller
@RequestMapping(RequestMappingPath.RECOMENDATIONS_API_BASE_URL)
public class RecommendationController {

	@Resource(name = Facades.RECOMMENDATION_FACADE)
	private RecommendationFacade recommendationFacade;

	@RequestMapping(UrlPart.RECOMENDATION_FOR_STATION)
	@ResponseBody
	public Song recommendSong(@PathVariable(PathVariables.ID) Long id) {
		return recommendationFacade.getRecommendationSong(id);

	}

}
