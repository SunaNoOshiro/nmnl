package com.suna.nmnl.controller.api;

import java.security.Principal;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import com.suna.nmnl.facade.RoleFacade;
import com.suna.nmnl.facade.StationFacade;
import com.suna.nmnl.facade.UserFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.model.Role;
import com.suna.nmnl.model.Station;
import com.suna.nmnl.model.User;
import com.suna.nmnl.util.ConstantUtil.Web;
import com.suna.nmnl.util.ConstantUtil.Web.PathVariables;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Beans.Facades;

@Controller
@RequestMapping(RequestMappingPath.STATIONS_API_BASE_URL)
public class StationController {

	private static final String NOT_DELETED_STATUS = "not deleted";

	private static final String DELETED_STATUS = "deleted";

	@Resource(name = Facades.USER_FACADE)
	private UserFacade userFacade;

	@Resource(name = Facades.ROLE_FACADE)
	private RoleFacade roleFacade;

	@Resource(name = Facades.STATION_FACADE)
	private StationFacade stationFacade;

	@RequestMapping(value = UrlPart.MY_STATIONS, method = RequestMethod.POST, headers = { Web.JSON_CONTENT_TYPE })
	@ResponseBody
	public Station addStation(@RequestBody @Valid Station station, Principal principal) {
		User user = userFacade.findByLogin(principal.getName());
		if (user != null) {
			if (station.getId() != null && stationFacade.exist(station.getId())) {
				return null;
			} else {
				station.setUser(user);
				station = stationFacade.save(station);
			}
		}

		return station;
	}

	@RequestMapping(value = UrlPart.MY_STATIONS_BY_ID, method = RequestMethod.PUT, headers = { Web.JSON_CONTENT_TYPE })
	@ResponseBody
	public Station updateStation(@PathVariable(PathVariables.ID) Long stationId, @RequestBody @Valid Station station,
			Principal principal) {

		if (stationFacade.exist(stationId) && station.getUser() != null
				&& station.getUser().getLogin().equals(principal.getName())) {
			stationFacade.save(station);
		}

		return station;
	}

	@RequestMapping(value = UrlPart.MY_STATIONS_BY_ID, method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteStation(@PathVariable(PathVariables.ID) Long stationId, Principal principal) {
		Station station = stationFacade.find(stationId);
		String deleteStatus = NOT_DELETED_STATUS;

		if (station != null && station.getUser() != null && station.getUser().getLogin().equals(principal.getName())) {
			stationFacade.delete(station);
			deleteStatus = DELETED_STATUS;
		}

		return deleteStatus;
	}

	@RequestMapping(value = UrlPart.MY_STATIONS, method = RequestMethod.GET)
	@ResponseBody
	public List<Station> getUserStations(Principal principal) {
		return stationFacade.findByUserLogin(principal.getName());
	}

	@RequestMapping(value = UrlPart.FIND_BY_USERS_ID, method = RequestMethod.GET)
	@ResponseBody
	public List<Station> getConcreteUserStations(@PathVariable(PathVariables.ID) Long userId) {
		return stationFacade.findByUserId(userId);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Station> getAllStations() {
		return stationFacade.findAllWithRates();
	}

	@RequestMapping(value = UrlPart.FIND_BY_ID, method = RequestMethod.GET)
	@ResponseBody
	public Station getUserStation(@PathVariable(PathVariables.ID) Long stationId) {
		return stationFacade.find(stationId);
	}

	@RequestMapping(value = UrlPart.FIND_BY_ID, method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteUserStation(@PathVariable(PathVariables.ID) Long stationId, Principal principal) {
		User user = userFacade.findByLogin(principal.getName());
		Role adminRole = roleFacade.findByName(RoleEnum.ROLE_ADMIN);

		if (user != null && user.getRoles() != null && user.getRoles().contains(adminRole)) {
			stationFacade.delete(stationId);
			return DELETED_STATUS;
		}
		return NOT_DELETED_STATUS;
	}
}
