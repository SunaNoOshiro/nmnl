package com.suna.nmnl.controller.api;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.facade.GenreFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.nmnl.model.Genre;
import com.suna.nmnl.util.ConstantUtil.Web.PathVariables;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Beans.Facades;

@Controller
@RequestMapping(RequestMappingPath.GENRES_API_BASE_URL)
public class GenreController {

	@Resource(name = Facades.GENRE_FACADE)
	private GenreFacade genreFacade;

	@RequestMapping(UrlPart.FIND_BY_QUERY)
	@ResponseBody
	public List<Genre> searchGenre(@PathVariable(PathVariables.QUERY) String query) {
		return genreFacade.findGenreByQuery(query);
	}
}
