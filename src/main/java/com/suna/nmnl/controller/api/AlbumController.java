package com.suna.nmnl.controller.api;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.facade.AlbumFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.nmnl.model.Album;
import com.suna.nmnl.util.ConstantUtil.Web.PathVariables;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Beans.Facades;

@Controller
@RequestMapping(RequestMappingPath.ALBUMS_API_BASE_URL)
public class AlbumController {

	@Resource(name = Facades.ALBUM_FACADE)
	private AlbumFacade albumFacade;

	@RequestMapping(value = UrlPart.FIND_BY_QUERY)
	@ResponseBody
	public List<Album> searchAlbum(@PathVariable(PathVariables.QUERY) String query) {
		return albumFacade.findAlbumsByQuery(query);

	}
}
