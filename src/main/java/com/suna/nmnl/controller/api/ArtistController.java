package com.suna.nmnl.controller.api;

import com.suna.nmnl.facade.ArtistFacade;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.util.ConstantUtil.Beans.Facades;
import com.suna.nmnl.util.ConstantUtil.Web.PathVariables;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping(RequestMappingPath.ARTISTS_API_BASE_URL)
public class ArtistController {

	@Resource(name = Facades.ARTIST_FACADE)
	private ArtistFacade artistFacade;

	@RequestMapping(UrlPart.FIND_BY_QUERY)
	@ResponseBody
	public List<Artist> searchArtist(@PathVariable(PathVariables.QUERY) String query) {
		return artistFacade.findArtistsByQuery(query);
	}
}
