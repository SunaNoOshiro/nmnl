package com.suna.nmnl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.suna.nmnl.util.ConstantUtil.Web.Page.*;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;

@Controller
public class LoginController {

	@RequestMapping(value = UrlPart.LOGIN_PAGE)
	public String login() {
		return LOGIN_PAGE;
	}
}
