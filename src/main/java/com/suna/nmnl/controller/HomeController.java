package com.suna.nmnl.controller;

import static com.suna.nmnl.util.ConstantUtil.Web.Page.HOME_PAGE;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.suna.nmnl.service.UserService;
import com.suna.nmnl.util.ConstantUtil.Web.RequestMappingPath.UrlPart;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

@Controller
public class HomeController {

	@Resource(name = Services.USER_SERVICE)
	private UserService userService;

	@RequestMapping(value = UrlPart.HOME, method = RequestMethod.GET)
	public String home() {
		return HOME_PAGE;
	}

}
