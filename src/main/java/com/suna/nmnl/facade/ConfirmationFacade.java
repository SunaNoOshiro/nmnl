package com.suna.nmnl.facade;

import com.suna.nmnl.model.Confirmation;

public interface ConfirmationFacade {
	
	Confirmation save(Confirmation confirmation);

	Confirmation findOne(String token);

	boolean exist(String token);

	void delete(Confirmation confirmation);
}
