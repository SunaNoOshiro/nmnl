package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.dto.AlbumDTO;

public interface AlbumFacade {

	List<Album> findAlbumsByQuery(String query);

	List<AlbumDTO> getAlbumsDataByQuery(String query);

	List<Album> findAlbumsByArtistId(Long id);

	List<AlbumDTO> getAlbumsDataByArtistId(Long id);

	Album findOne(Long id);

	AlbumDTO getAlbumDataById(Long id);

	AbstractConverter<Album, AlbumDTO> getAlbumModelConverter();

	AbstractConverter<AlbumDTO, Album> getAlbumDTOConverter();
}
