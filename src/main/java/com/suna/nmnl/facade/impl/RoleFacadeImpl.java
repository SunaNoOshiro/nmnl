package com.suna.nmnl.facade.impl;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.facade.RoleFacade;
import com.suna.nmnl.model.Role;
import com.suna.nmnl.service.RoleService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

import javax.annotation.Resource;
import java.util.List;

public class RoleFacadeImpl implements RoleFacade {

    @Resource(name = Services.ROLE_SERVICE)
    private RoleService roleService;

    @Override
    public Role findOne(Long id) {
        return roleService.findOne(id);
    }

    @Override
    public Role findByName(RoleEnum name) {
        return roleService.findByName(name);
    }

    @Override
    public Role save(Role role) {
        return roleService.save(role);
    }

    @Override
    public void delete(Role role) {
        roleService.delete(role);
    }

    @Override
    public List<Role> findAll() {
        return roleService.findAll();
    }
}
