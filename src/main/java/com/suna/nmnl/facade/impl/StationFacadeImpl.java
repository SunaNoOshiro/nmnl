package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.StationFacade;
import com.suna.nmnl.model.Station;
import com.suna.nmnl.model.dto.StationDTO;
import com.suna.nmnl.service.StationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class StationFacadeImpl implements StationFacade {

	@Resource(name = Converters.STATION_MODEL_CONVERTER)
	private AbstractConverter<Station, StationDTO> stationModelConverter;

	@Resource(name = Converters.STATION_DTO_CONVERTER)
	private AbstractConverter<StationDTO, Station> stationDTOConverter;

	@Resource(name = Services.STATION_SERVICE)
	private StationService stationService;

	@Override
	public Station save(Station station) {
		return stationService.save(station);
	}

	@Override
	public Station save(StationDTO stationData) {
		Station station = stationDTOConverter.convert(stationData);
		return stationService.save(station);
	}

	@Override
	public Station find(Long id) {
		return stationService.find(id);
	}

	@Override
	public Station findWithRates(Long id) {
		return stationService.findWithRates(id);
	}

	@Override
	public StationDTO getStationData(Long id) {
		Station station = stationService.find(id);
		return stationModelConverter.convert(station);
	}

	@Override
	public List<Station> findAll() {
		return stationService.findAll();
	}

	@Override
	public List<Station> findAllWithRates() {
		return stationService.findAllWithRates();
	}

	@Override
	public List<StationDTO> getAllData() {
		List<Station> stations = stationService.findAll();
		return convertStations(stations);
	}

	@Override
	public void delete(Station station) {
		stationService.delete(station);
	}

	@Override
	public void delete(Long stationId) {
		stationService.delete(stationId);
	}

	@Override
	public void delete(StationDTO stationData) {
		stationService.delete(stationData.getId());
	}

	@Override
	public boolean exist(Long stationId) {
		return stationService.exist(stationId);
	}

	@Override
	public List<Station> findByUserLogin(String login) {
		return stationService.findByUserLogin(login);
	}

	@Override
	public List<StationDTO> getStationsDataByUserLogin(String login) {
		List<Station> stations = stationService.findByUserLogin(login);
		return convertStations(stations);
	}

	@Override
	public List<Station> findByUserId(Long userId) {
		return stationService.findByUserId(userId);
	}

	@Override
	public List<StationDTO> getStationsDataByUserId(Long userId) {
		List<Station> stations = stationService.findByUserId(userId);
		return convertStations(stations);
	}

	@Override
	public AbstractConverter<Station, StationDTO> getStationModelConverter() {
		return stationModelConverter;
	}

	@Override
	public AbstractConverter<StationDTO, Station> getStationDTOConverter() {
		return stationDTOConverter;
	}

	private List<StationDTO> convertStations(List<Station> stations) {
		List<StationDTO> stationsData = new ArrayList<>();

		for (Station station : stations) {
			stationsData.add(stationModelConverter.convert(station));
		}

		return stationsData;
	}
}
