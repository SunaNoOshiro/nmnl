package com.suna.nmnl.facade.impl;

import javax.annotation.Resource;

import com.suna.nmnl.facade.ConfirmationFacade;
import com.suna.nmnl.model.Confirmation;
import com.suna.nmnl.service.ConfirmationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class ConfirmationFacadeImpl implements ConfirmationFacade {

	@Resource(name = Services.CONFIRMATION_SERVICE)
	private ConfirmationService confirmationService;

	@Override
	public Confirmation save(Confirmation confirmation) {
		return confirmationService.save(confirmation);
	}

	@Override
	public Confirmation findOne(String token) {
		return confirmationService.findOne(token);
	}

	@Override
	public boolean exist(String token) {
		return confirmationService.exist(token);
	}

	@Override
	public void delete(Confirmation confirmation) {
		confirmationService.delete(confirmation);
	}

}
