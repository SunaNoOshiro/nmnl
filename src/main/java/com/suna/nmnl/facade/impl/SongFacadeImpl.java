package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.SongFacade;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.SongDTO;
import com.suna.nmnl.service.SongService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class SongFacadeImpl implements SongFacade {

	@Resource(name = Converters.SONG_MODEL_CONVERTER)
	private AbstractConverter<Song, SongDTO> songModelConverter;

	@Resource(name = Converters.SONG_DTO_CONVERTER)
	private AbstractConverter<SongDTO, Song> songDTOConverter;

	@Resource(name = Services.SONG_SERVICE)
	private SongService songService;

	@Override
	public Song findOne(Long id) {
		return songService.findOne(id);
	}

	@Override
	public SongDTO getSongData(Long id) {
		Song song = songService.findOne(id);
		return songModelConverter.convert(song);
	}

	@Override
	public Song fetch(Long id) {
		return songService.fetch(id);
	}

	@Override
	public Song save(Song song) {
		return songService.save(song);
	}

	@Override
	public Song save(SongDTO songData) {
		Song song = songDTOConverter.convert(songData);
		return songService.save(song);
	}

	@Override
	public void delete(Song song) {
		songService.delete(song);
	}

	@Override
	public void delete(SongDTO songData) {
		Song song = songDTOConverter.convert(songData);
		songService.delete(song);
	}

	@Override
	public Song findSongWithGenres(Long id) {
		return songService.findSongWithGenres(id);
	}

	@Override
	public List<Song> findAll() {
		return songService.findAll();
	}

	@Override
	public List<Song> findSongsByQuery(String query) {
		return songService.findSongsByQuery(query);
	}

	@Override
	public List<SongDTO> getSongsDataByQuery(String query) {
		List<Song> songs = songService.findSongsByQuery(query);
		return convertSongs(songs);
	}

	@Override
	public List<Song> findSongsByStation(Long id) {
		return songService.findSongsByStation(id);
	}

	@Override
	public List<SongDTO> getSongsDataByStation(Long id) {
		List<Song> songs = songService.findSongsByStation(id);
		return convertSongs(songs);
	}

	@Override
	public List<Song> findSongsByAlbum(Long id) {
		return songService.findSongsByAlbum(id);
	}

	@Override
	public List<SongDTO> getSongsDataByAlbum(Long id) {
		List<Song> songs = songService.findSongsByAlbum(id);
		return convertSongs(songs);
	}

	@Override
	public List<Song> findSongsByGenre(Long id) {
		return songService.findSongsByGenre(id);
	}

	@Override
	public List<SongDTO> getSongsDataByGenre(Long id) {
		List<Song> songs = songService.findSongsByGenre(id);
		return convertSongs(songs);
	}

	@Override
	public boolean playedToday(Long id) {
		return songService.playedToday(id);
	}

	@Override
	public AbstractConverter<Song, SongDTO> getSongModelConverter() {
		return songModelConverter;
	}

	@Override
	public AbstractConverter<SongDTO, Song> getSongDTOConverter() {
		return songDTOConverter;
	}

	private List<SongDTO> convertSongs(List<Song> songs) {
		List<SongDTO> songsData = new ArrayList<>();

		for (Song song : songs) {
			songsData.add(songModelConverter.convert(song));
		}
		return songsData;
	}

}
