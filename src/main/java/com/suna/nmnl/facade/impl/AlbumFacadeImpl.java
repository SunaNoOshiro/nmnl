package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.AlbumFacade;
import com.suna.nmnl.model.Album;
import com.suna.nmnl.model.dto.AlbumDTO;
import com.suna.nmnl.service.AlbumService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class AlbumFacadeImpl implements AlbumFacade {

	@Resource(name = Converters.ALBUM_MODEL_CONVERTER)
	private AbstractConverter<Album, AlbumDTO> albumModelConverter;

	@Resource(name = Converters.ALBUM_DTO_CONVERTER)
	private AbstractConverter<AlbumDTO, Album> albumDTOConverter;

	@Resource(name = Services.ALBUM_SERVICE)
	private AlbumService albumService;

	@Override
	public List<Album> findAlbumsByQuery(String query) {
		return albumService.findAlbumsByQuery(query);
	}

	@Override
	public List<AlbumDTO> getAlbumsDataByQuery(String query) {
		List<Album> albums = albumService.findAlbumsByQuery(query);
		List<AlbumDTO> albumsData = new ArrayList<>();

		for (Album album : albums) {
			albumsData.add(albumModelConverter.convert(album));
		}

		return albumsData;
	}

	@Override
	public List<Album> findAlbumsByArtistId(Long id) {
		return albumService.findAlbumsByArtistId(id);
	}

	@Override
	public List<AlbumDTO> getAlbumsDataByArtistId(Long id) {
		List<Album> albums = albumService.findAlbumsByArtistId(id);
		List<AlbumDTO> albumsData = new ArrayList<>();

		for (Album album : albums) {
			albumsData.add(albumModelConverter.convert(album));
		}

		return albumsData;
	}

	@Override
	public Album findOne(Long id) {
		return albumService.findOne(id);
	}

	@Override
	public AlbumDTO getAlbumDataById(Long id) {
		Album album = albumService.findOne(id);
		return albumModelConverter.convert(album);
	}

	@Override
	public AbstractConverter<Album, AlbumDTO> getAlbumModelConverter() {
		return albumModelConverter;
	}

	@Override
	public AbstractConverter<AlbumDTO, Album> getAlbumDTOConverter() {
		return albumDTOConverter;
	}

}
