package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.GenreFacade;
import com.suna.nmnl.model.Genre;
import com.suna.nmnl.model.dto.GenreDTO;
import com.suna.nmnl.service.GenreService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class GenreFacadeImpl implements GenreFacade {

	@Resource(name = Converters.GENRE_MODEL_CONVERTER)
	private AbstractConverter<Genre, GenreDTO> genreModelConverter;

	@Resource(name = Converters.GENRE_DTO_CONVERTER)
	private AbstractConverter<GenreDTO, Genre> genreDTOConverter;

	@Resource(name = Services.GENRE_SERVICE)
	private GenreService genreService;

	@Override
	public List<Genre> findGenreByQuery(String query) {
		return genreService.findGenreByQuery(query);
	}

	@Override
	public List<GenreDTO> getGenreDataByQuery(String query) {
		List<Genre> genres = genreService.findGenreByQuery(query);
		List<GenreDTO> genresData = new ArrayList<>();

		for (Genre genre : genres) {
			genresData.add(genreModelConverter.convert(genre));
		}

		return genresData;
	}

	@Override
	public Genre findOne(Long id) {
		return genreService.findOne(id);
	}

	@Override
	public GenreDTO getGenreData(Long id) {
		Genre genre = genreService.findOne(id);
		return genreModelConverter.convert(genre);
	}

	@Override
	public AbstractConverter<Genre, GenreDTO> getGenreModelConverter() {
		return genreModelConverter;
	}

	@Override
	public AbstractConverter<GenreDTO, Genre> getGenreDTOConverter() {
		return genreDTOConverter;
	}

}
