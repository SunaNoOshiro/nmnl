package com.suna.nmnl.facade.impl;

import javax.annotation.Resource;

import com.suna.nmnl.facade.MailConfirmationFacade;
import com.suna.nmnl.model.Confirmation;
import com.suna.nmnl.model.User;
import com.suna.nmnl.service.MailConfirmationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class MailConfirmationFacadeImpl implements MailConfirmationFacade {

	@Resource(name = Services.MAIL_CONFIRMATION_SERVICE)
	private MailConfirmationService mailConfirmationService;

	@Override
	public void sendEmailAndSaveConfirmation(User user) {
		mailConfirmationService.sendEmailAndSaveConfirmation(user);
	}

	@Override
	public Confirmation findOne(String token) {
		return mailConfirmationService.findOne(token);
	}

	@Override
	public void delete(Confirmation confirmation) {
		mailConfirmationService.delete(confirmation);
	}

	@Override
	public boolean isValid(String token) {
		return mailConfirmationService.isValid(token);
	}

	@Override
	public void delete(String token) {
		mailConfirmationService.delete(token);
	}

}
