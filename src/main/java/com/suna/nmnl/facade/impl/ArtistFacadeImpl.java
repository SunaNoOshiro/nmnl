package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.ArtistFacade;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.dto.ArtistDTO;
import com.suna.nmnl.service.ArtistService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class ArtistFacadeImpl implements ArtistFacade {

	@Resource(name = Converters.ARTIST_MODEL_CONVERTER)
	private AbstractConverter<Artist, ArtistDTO> artistModelConverter;

	@Resource(name = Converters.ARTIST_DTO_CONVERTER)
	private AbstractConverter<ArtistDTO, Artist> artistDTOConverter;

	@Resource(name = Services.ARTIST_SERVICE)
	private ArtistService artistService;

	@Override
	public List<Artist> findArtistsByQuery(String query) {
		return artistService.findArtistsByQuery(query);
	}

	@Override
	public List<ArtistDTO> getArtistsDataByQuery(String query) {
		List<Artist> artists = artistService.findArtistsByQuery(query);
		List<ArtistDTO> artistsData = new ArrayList<>();

		for (Artist artist : artists) {
			artistsData.add(artistModelConverter.convert(artist));
		}

		return artistsData;
	}

	@Override
	public Artist findOne(Long id) {
		return artistService.findOne(id);
	}

	@Override
	public ArtistDTO getArtistDataById(Long id) {
		Artist artist = artistService.findOne(id);
		return artistModelConverter.convert(artist);
	}

	@Override
	public AbstractConverter<Artist, ArtistDTO> getArtistModelConverter() {
		return artistModelConverter;
	}

	@Override
	public AbstractConverter<ArtistDTO, Artist> getArtistDTOConverter() {
		return artistDTOConverter;
	}

}
