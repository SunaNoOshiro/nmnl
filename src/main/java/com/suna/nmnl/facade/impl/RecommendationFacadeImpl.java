package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.RecommendationFacade;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.SongDTO;
import com.suna.nmnl.service.RecommendationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class RecommendationFacadeImpl implements RecommendationFacade {

	@Resource(name = Converters.SONG_MODEL_CONVERTER)
	private AbstractConverter<Song, SongDTO> songModelConverter;

	@Resource(name = Services.RECOMMENDATION_SERVICE)
	private RecommendationService recommendationService;

	@Override
	public List<Song> getRecommendationSongs(Long stationId) {
		return recommendationService.getRecommendationSongs(stationId);
	}

	@Override
	public Song getRecommendationSong(Long stationId) {
		return recommendationService.getRecommendationSong(stationId);
	}

	@Override
	public List<SongDTO> getRecommendationSongsData(Long stationId) {
		List<Song> songs = recommendationService.getRecommendationSongs(stationId);
		List<SongDTO> songsData = new ArrayList<>();

		for (Song song : songs) {
			songsData.add(songModelConverter.convert(song));
		}

		return songsData;
	}

	@Override
	public SongDTO getRecommendationSongData(Long stationId) {
		Song song = recommendationService.getRecommendationSong(stationId);
		return songModelConverter.convert(song);
	}

}
