package com.suna.nmnl.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.facade.UserFacade;
import com.suna.nmnl.model.User;
import com.suna.nmnl.model.dto.UserDTO;
import com.suna.nmnl.service.UserService;
import com.suna.nmnl.util.ConstantUtil.Beans.Converters;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class UserFacadeImpl implements UserFacade {

	@Resource(name = Converters.USER_MODEL_CONVERTER)
	private AbstractConverter<User, UserDTO> userModelConverter;

	@Resource(name = Converters.USER_DTO_CONVERTER)
	private AbstractConverter<UserDTO, User> userDTOConverter;

	@Resource(name = Services.USER_SERVICE)
	private UserService userService;

	@Override
	public User save(User user) {
		return userService.save(user);
	}

	@Override
	public User save(UserDTO userData) {
		User user = userDTOConverter.convert(userData);
		return userService.save(user);
	}

	@Override
	public User update(User user) {
		return userService.update(user);
	}

	@Override
	public User update(UserDTO userData) {
		User user = userDTOConverter.convert(userData);
		return userService.update(user);
	}

	@Override
	public void delete(User user) {
		userService.delete(user);
	}

	@Override
	public void delete(UserDTO userData) {
		User user = userDTOConverter.convert(userData);
		userService.delete(user);
	}

	@Override
	public User findByLogin(String login) {
		return userService.findByLogin(login);
	}

	@Override
	public UserDTO getUserDataByLogin(String login) {
		User user = userService.findByLogin(login);
		return userModelConverter.convert(user);
	}

	@Override
	public User findOne(Long id) {
		return userService.findOne(id);
	}

	@Override
	public User findOneWithStations(Long id) {
		return userService.findOneWithStations(id);
	}

	@Override
	public UserDTO getUserDataById(Long id) {
		User user = userService.findOne(id);
		return userModelConverter.convert(user);
	}

	@Override
	public User findByEmail(String email) {
		return userService.findByEmail(email);
	}

	@Override
	public UserDTO getUserDataByEmail(String email) {
		User user = userService.findByEmail(email);
		return userModelConverter.convert(user);
	}

	@Override
	public User findByToken(String token) {
		return userService.findByToken(token);
	}

	@Override
	public UserDTO getUserDataByToken(String token) {
		User user = userService.findByToken(token);
		return userModelConverter.convert(user);
	}

	@Override
	public List<User> findAll() {
		return userService.findAll();
	}

	@Override
	public List<UserDTO> getAllUsersData() {
		List<User> users = userService.findAll();
		return convertUsers(users);
	}

	@Override
	public boolean exist(Long userId) {
		return userService.exist(userId);
	}

	@Override
	public AbstractConverter<User, UserDTO> getUserModelConverter() {
		return userModelConverter;
	}

	@Override
	public AbstractConverter<UserDTO, User> getUserDTOConverter() {
		return userDTOConverter;
	}

	private List<UserDTO> convertUsers(List<User> users) {
		List<UserDTO> usersData = new ArrayList<>();

		for (User user : users) {
			UserDTO userData = userModelConverter.convert(user);
			usersData.add(userData);
		}
		return usersData;
	}
}
