package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.SongDTO;

public interface RecommendationFacade {

	List<Song> getRecommendationSongs(Long stationId);

	Song getRecommendationSong(Long stationId);
	
	List<SongDTO> getRecommendationSongsData(Long stationId);

	SongDTO getRecommendationSongData(Long stationId);
}
