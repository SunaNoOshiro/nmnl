package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.Song;
import com.suna.nmnl.model.dto.SongDTO;

public interface SongFacade {

	Song findOne(Long id);

	SongDTO getSongData(Long id);

	Song fetch(Long id);

	Song save(Song song);

	Song save(SongDTO songData);

	void delete(Song song);

	void delete(SongDTO songData);

	Song findSongWithGenres(Long id);

	List<Song> findAll();

	List<Song> findSongsByQuery(String query);

	List<SongDTO> getSongsDataByQuery(String query);

	List<Song> findSongsByStation(Long id);

	List<SongDTO> getSongsDataByStation(Long id);

	List<Song> findSongsByAlbum(Long id);

	List<SongDTO> getSongsDataByAlbum(Long id);

	List<Song> findSongsByGenre(Long id);

	List<SongDTO> getSongsDataByGenre(Long id);

	boolean playedToday(Long id);

	AbstractConverter<Song, SongDTO> getSongModelConverter();

	AbstractConverter<SongDTO, Song> getSongDTOConverter();
}
