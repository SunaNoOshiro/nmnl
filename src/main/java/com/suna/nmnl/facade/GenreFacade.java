package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.Genre;
import com.suna.nmnl.model.dto.GenreDTO;

public interface GenreFacade {

	List<Genre> findGenreByQuery(String query);

	List<GenreDTO> getGenreDataByQuery(String query);

	Genre findOne(Long id);

	GenreDTO getGenreData(Long id);

	AbstractConverter<Genre, GenreDTO> getGenreModelConverter();

	AbstractConverter<GenreDTO, Genre> getGenreDTOConverter();
}
