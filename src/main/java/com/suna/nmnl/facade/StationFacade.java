package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.Station;
import com.suna.nmnl.model.dto.StationDTO;

public interface StationFacade {

	Station save(Station station);

	Station save(StationDTO stationData);

	Station find(Long id);

	Station findWithRates(Long id);

	StationDTO getStationData(Long id);

	List<Station> findAll();

	List<Station> findAllWithRates();

	List<StationDTO> getAllData();

	void delete(Station station);

	void delete(Long stationId);

	void delete(StationDTO stationData);

	boolean exist(Long stationId);

	List<Station> findByUserLogin(String login);

	List<StationDTO> getStationsDataByUserLogin(String login);

	List<Station> findByUserId(Long userId);

	List<StationDTO> getStationsDataByUserId(Long userId);

	AbstractConverter<Station, StationDTO> getStationModelConverter();

	AbstractConverter<StationDTO, Station> getStationDTOConverter();
}
