package com.suna.nmnl.facade;

import com.suna.nmnl.model.Confirmation;
import com.suna.nmnl.model.User;

public interface MailConfirmationFacade {

	void sendEmailAndSaveConfirmation(User user);

	Confirmation findOne(String token);

	void delete(Confirmation confirmation);

	boolean isValid(String token);

	void delete(String token);
}
