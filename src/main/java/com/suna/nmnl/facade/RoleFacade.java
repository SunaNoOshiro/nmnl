package com.suna.nmnl.facade;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.model.Role;

import java.util.List;

public interface RoleFacade {

    Role findOne(Long id);

    Role save(Role role);

    void delete(Role role);

    List<Role> findAll();

    Role findByName(RoleEnum name);
}
