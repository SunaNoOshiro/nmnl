package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.Artist;
import com.suna.nmnl.model.dto.ArtistDTO;

public interface ArtistFacade {

	List<Artist> findArtistsByQuery(String query);

	List<ArtistDTO> getArtistsDataByQuery(String query);

	Artist findOne(Long id);

	ArtistDTO getArtistDataById(Long id);

	AbstractConverter<Artist, ArtistDTO> getArtistModelConverter();

	AbstractConverter<ArtistDTO, Artist> getArtistDTOConverter();
}
