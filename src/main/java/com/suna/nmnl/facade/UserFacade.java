package com.suna.nmnl.facade;

import java.util.List;

import com.suna.nmnl.converter.AbstractConverter;
import com.suna.nmnl.model.User;
import com.suna.nmnl.model.dto.UserDTO;

public interface UserFacade {

	User save(User user);

	User save(UserDTO userData);

	User update(User user);

	User update(UserDTO userData);

	void delete(User user);

	void delete(UserDTO userData);

	User findByLogin(String login);

	UserDTO getUserDataByLogin(String login);

	User findOne(Long id);

	User findOneWithStations(Long id);

	UserDTO getUserDataById(Long id);

	User findByEmail(String email);

	UserDTO getUserDataByEmail(String email);

	User findByToken(String token);

	UserDTO getUserDataByToken(String token);

	List<User> findAll();

	List<UserDTO> getAllUsersData();

	boolean exist(Long userId);

	AbstractConverter<User, UserDTO> getUserModelConverter();

	AbstractConverter<UserDTO, User> getUserDTOConverter();
}
