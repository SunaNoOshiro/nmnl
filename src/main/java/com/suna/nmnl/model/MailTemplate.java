package com.suna.nmnl.model;

public class MailTemplate {

	private String subject;

	private String velocityURL;

	private String from;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getVelocityURL() {
		return velocityURL;
	}

	public void setVelocityURL(String velocityURL) {
		this.velocityURL = velocityURL;
	}
}
