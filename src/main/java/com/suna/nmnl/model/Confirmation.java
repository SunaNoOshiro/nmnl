package com.suna.nmnl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Confirmation {
	@Id
	private String token;

	@Column(name = "expires_end")
	private Date expiresEnd;

	@OneToOne
	private User user;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiresEnd() {
		return expiresEnd;
	}

	public void setExpiresEnd(Date expiresEnd) {
		this.expiresEnd = expiresEnd;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
