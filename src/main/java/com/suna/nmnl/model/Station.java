package com.suna.nmnl.model;

import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Station extends AbstractModel {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private String name;

	@OneToMany
	@JoinTable(name = "songs_rate", joinColumns = @JoinColumn(name = "station_id") , inverseJoinColumns = @JoinColumn(name = "rate_id") )
	@MapKeyJoinColumn(name = "song_id")
	private Map<Song, Rate> songRates;
	
	@OneToMany
	@JoinTable(name = "genre_rate", joinColumns = @JoinColumn(name = "station_id") , inverseJoinColumns = @JoinColumn(name = "rate_id") )
	@MapKeyJoinColumn(name = "genre_id")
	private Map<Genre, Rate> genreRates;
	
	@OneToMany
	@JoinTable(name = "albums_rate", joinColumns = @JoinColumn(name = "station_id") , inverseJoinColumns = @JoinColumn(name = "rate_id") )
	@MapKeyJoinColumn(name = "album_id")
	private Map<Album, Rate> albumRates;
	
	@OneToMany
	@JoinTable(name = "last_played", joinColumns = @JoinColumn(name = "station_id") , inverseJoinColumns = @JoinColumn(name = "play_type_id") )
	@MapKeyJoinColumn(name = "song_id")
	private Map<Song, PlayingType> lastPlayed;
	

	@ManyToOne
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Song, Rate> getSongRates() {
		return songRates;
	}

	public void setSongRates(Map<Song, Rate> songRates) {
		this.songRates = songRates;
	}

	public Map<Genre, Rate> getGenreRates() {
		return genreRates;
	}

	public void setGenreRates(Map<Genre, Rate> genreRates) {
		this.genreRates = genreRates;
	}

	public Map<Album, Rate> getAlbumRates() {
		return albumRates;
	}

	public Map<Song, PlayingType> getLastPlayed() {
		return lastPlayed;
	}

	public void setLastPlayed(Map<Song, PlayingType> lastPlayed) {
		this.lastPlayed = lastPlayed;
	}

	public void setAlbumRates(Map<Album, Rate> albumRates) {
		this.albumRates = albumRates;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Station other = (Station) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
