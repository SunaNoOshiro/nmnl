package com.suna.nmnl.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.suna.nmnl.annotation.UniqueEmail;
import com.suna.nmnl.annotation.UniqueLogin;
import com.suna.nmnl.enums.Gender;

@Entity(name = "users")
@JsonIgnoreProperties({ "password", "active", "email", "roles" })
public class User extends AbstractModel{

	@Id
	@GeneratedValue
	private Long id;

	@Size(min = 3, message = "${hibernate.validation.user.login.size}")
	@UniqueLogin(message = "${hibernate.validation.user.login.unique}")
	private String login;

	@Email(message = "${hibernate.validation.user.email.invalid}")
	@Size(min = 3, message = "${hibernate.validation.user.email.size}")
	@UniqueEmail(message = "${hibernate.validation.user.email.unique}")
	private String email;

	@Size(min = 6, message = "${hibernate.validation.user.password.size}")
	private String password;

	@Size(min = 3, message = "${hibernate.validation.user.firstname.size}")
	@Column(name = "first_name")
	private String firstName;

	@Size(min = 3, message = "${hibernate.validation.user.lastname.size}")
	@Column(name = "last_name")
	private String lastName;

	private Boolean active;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable
	private List<Role> roles;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Station> stations;

	private String countryCode;

	private Date birthday;

	private String language;

	private Gender gender;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Station> getStations() {
		return stations;
	}

	public void setStations(List<Station> stations) {
		this.stations = stations;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}
}
