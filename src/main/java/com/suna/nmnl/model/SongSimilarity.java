package com.suna.nmnl.model;

public class SongSimilarity {
	
	private Long leftSongId;
	private Long rightSongId;
	private Float similarity;

	public Long getLeftSongId() {
		return leftSongId;
	}

	public void setLeftSongId(Long leftSongId) {
		this.leftSongId = leftSongId;
	}

	public Long getRightSongId() {
		return rightSongId;
	}

	public void setRightSongId(Long rightSongId) {
		this.rightSongId = rightSongId;
	}

	public Float getSimilarity() {
		return similarity;
	}

	public void setSimilarity(Float similarity) {
		this.similarity = similarity;
	}
}
