package com.suna.nmnl.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import com.suna.nmnl.annotation.UniqueSoundCloudId;

@Entity
public class Song extends AbstractModel implements Comparable<Song> {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "soundcloud_id")
	@UniqueSoundCloudId(message = "${hibernate.validation.song.soundcloudid.unique}")
	private Long soundcloudId;

	@Column(name = "stream_url")
	private String streamUrl;

	@Column(name = "download_url")
	private String downloadUrl;

	@Column(name = "artwork_url")
	private String artworkUrl;

	private String uri;

	@ManyToMany(fetch = FetchType.LAZY)
	private List<Genre> genres;

	private Float bpm;

	private Long duration;

	@ManyToOne
	private Album album;

	@ManyToOne
	private Artist artist;

	@ManyToOne
	private SongCluster cluster;

	@Lob
	@Type(type = "org.hibernate.type.StringClobType")
	@Column(length = Integer.MAX_VALUE)
	private String description;

	private String title;

	public String getArtworkUrl() {
		return artworkUrl;
	}

	public void setArtworkUrl(String artworkUrl) {
		this.artworkUrl = artworkUrl;
	}

	public String getStreamUrl() {
		return streamUrl;
	}

	public void setStreamUrl(String streamUrl) {
		this.streamUrl = streamUrl;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public Float getBpm() {
		return bpm;
	}

	public void setBpm(Float bpm) {
		this.bpm = bpm;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSoundcloudId() {
		return soundcloudId;
	}

	public void setSoundcloudId(Long soundcloudId) {
		this.soundcloudId = soundcloudId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public SongCluster getCluster() {
		return cluster;
	}

	public void setCluster(SongCluster cluster) {
		this.cluster = cluster;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((soundcloudId == null) ? 0 : soundcloudId.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Song other = (Song) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (soundcloudId == null) {
			if (other.soundcloudId != null)
				return false;
		} else if (!soundcloudId.equals(other.soundcloudId))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public int compareTo(Song o) {
		return this.id.compareTo(o.id);
	}
}
