package com.suna.nmnl.model.dto;

import java.util.List;

public class ArtistDTO extends AbstractDTO {

	private Long id;

	private List<Integer> albums;

	private String name;

	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Integer> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Integer> albums) {
		this.albums = albums;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
