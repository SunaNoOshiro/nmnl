package com.suna.nmnl.model.dto;

import java.util.List;

public class AlbumDTO extends AbstractDTO {

	private Long id;

	private String name;

	private Long releaseDate;

	private List<Integer> songs;

	private String description;

	private ArtistDTO artist;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Long releaseDate) {
		this.releaseDate = releaseDate;
	}

	public List<Integer> getSongs() {
		return songs;
	}

	public void setSongs(List<Integer> songs) {
		this.songs = songs;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArtistDTO getArtist() {
		return artist;
	}

	public void setArtist(ArtistDTO artist) {
		this.artist = artist;
	}

}
