package com.suna.nmnl.model.dto;

import java.util.List;

public class SongDTO extends AbstractDTO {

	private Long id;

	private Long soundcloudId;

	private String streamUrl;

	private String downloadUrl;

	private String artworkUrl;

	private String uri;

	private List<Integer> genres;

	private Float bpm;

	private Long duration;

	private Long album;

	private Long artist;

	private String description;

	private String title;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSoundcloudId() {
		return soundcloudId;
	}

	public void setSoundcloudId(Long soundcloudId) {
		this.soundcloudId = soundcloudId;
	}

	public String getStreamUrl() {
		return streamUrl;
	}

	public void setStreamUrl(String streamUrl) {
		this.streamUrl = streamUrl;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getArtworkUrl() {
		return artworkUrl;
	}

	public void setArtworkUrl(String artworkUrl) {
		this.artworkUrl = artworkUrl;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<Integer> getGenres() {
		return genres;
	}

	public void setGenres(List<Integer> genres) {
		this.genres = genres;
	}

	public Float getBpm() {
		return bpm;
	}

	public void setBpm(Float bpm) {
		this.bpm = bpm;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getAlbum() {
		return album;
	}

	public void setAlbum(Long album) {
		this.album = album;
	}

	public Long getArtist() {
		return artist;
	}

	public void setArtist(Long artist) {
		this.artist = artist;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
