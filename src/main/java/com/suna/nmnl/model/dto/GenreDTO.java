package com.suna.nmnl.model.dto;

import java.util.List;

public class GenreDTO extends AbstractDTO {

	private Long id;

	private String name;

	private List<Integer> songs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getSongs() {
		return songs;
	}

	public void setSongs(List<Integer> songs) {
		this.songs = songs;
	}

}
