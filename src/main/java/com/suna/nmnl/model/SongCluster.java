package com.suna.nmnl.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class SongCluster {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private SongCluster parent;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SongCluster getParent() {
		return parent;
	}

	public void setParent(SongCluster parent) {
		this.parent = parent;
	}

}
