package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Genre;
import com.suna.nmnl.repository.GenreRepository;
import com.suna.nmnl.service.GenreService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class GenreServiceImpl implements GenreService {

	@Resource(name = Repositories.GENRE_REPOSITORY)
	private GenreRepository genreRepository;

	@Override
	public List<Genre> findGenreByQuery(String query) {
		return genreRepository.findGenreByQuery(query == null ? "" : query.toLowerCase());
	}

	@Override
	public Genre findOne(Long id) {
		return genreRepository.findOne(id);
	}

}
