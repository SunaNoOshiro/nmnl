package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.model.Role;
import com.suna.nmnl.repository.RoleRepository;
import com.suna.nmnl.service.RoleService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class RoleServiceImpl implements RoleService {

	@Resource(name = Repositories.ROLE_REPOSITORY)
	private RoleRepository roleRepository;

	@Override
	public Role findOne(Long id) {
		return roleRepository.findOne(id);
	}

	@Override
	public Role findByName(RoleEnum name) {
		return roleRepository.findByName(name);
	}

	@Override
	public Role save(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public void delete(Role role) {
		roleRepository.delete(role);

	}

	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}

}
