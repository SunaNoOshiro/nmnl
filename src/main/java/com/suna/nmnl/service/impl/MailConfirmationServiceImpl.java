package com.suna.nmnl.service.impl;

import static org.springframework.ui.velocity.VelocityEngineUtils.mergeTemplateIntoString;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.suna.nmnl.model.Confirmation;
import com.suna.nmnl.model.MailTemplate;
import com.suna.nmnl.model.User;
import com.suna.nmnl.repository.ConfirmationRepository;
import com.suna.nmnl.service.MailConfirmationService;
import com.suna.nmnl.util.ConstantUtil.Properties;
import com.suna.nmnl.util.ConstantUtil.Beans.MailTemplates;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class MailConfirmationServiceImpl implements MailConfirmationService {

	private static final boolean ENABLE_MULTIPART = true;
	private static final boolean ENABLE_HTML_TYPE = true;
	private static final String ENCODING = "UTF-8";

	private static final String CONFIRMATION_ATTRIBUTE = "confirmation";
	private static final String CONFIRMATION_LINK_ATTRIBUTE = "confirmation_link";

	@Resource(name = MailTemplates.MAIL_SENDER)
	private JavaMailSender javaMailSender;

	@Resource(name = MailTemplates.CONFIRMATION_MAIL_TEMPLATE)
	private MailTemplate mailTemplate;

	@Resource(name = MailTemplates.VELOСITY_ENGINE)
	private VelocityEngine velocityEngine;

	@Resource(name = Repositories.CONFIRMATION_REPOSITORY)
	private ConfirmationRepository confirmationRepository;

	@Override
	public Confirmation findOne(String token) {
		return confirmationRepository.findOne(token);
	}

	@Override
	public void delete(Confirmation confirmation) {
		confirmationRepository.delete(confirmation);
	}

	@Override
	public void delete(String token) {
		confirmationRepository.delete(token);
	}

	@Override
	public boolean isValid(String token) {
		boolean isValid = false;
		Confirmation confirmation = confirmationRepository.findOne(token);

		if (isValidConfirmationDate(confirmation)) {
			isValid = true;
		}

		return isValid;
	}

	@Override
	public void sendEmailAndSaveConfirmation(User user) {
		if (user != null) {
			Confirmation confirmation = createConfirmationForUser(user);
			sendConfirmationEmail(user, confirmation);
		}
	}

	private Confirmation createConfirmationForUser(User user) {
		Confirmation confirmation = new Confirmation();
		confirmation.setUser(user);
		confirmation.setToken(createToken(user));
		confirmation.setExpiresEnd(calculateExpiresEnd(new Date()));

		return confirmationRepository.save(confirmation);
	}

	private void sendConfirmationEmail(User user, Confirmation confirmation) {
		MimeMessage message = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, ENABLE_MULTIPART);
			helper.setTo(user.getEmail());
			helper.setFrom(mailTemplate.getFrom());
			helper.setSubject(mailTemplate.getSubject());
			helper.setText(createText(confirmation), ENABLE_HTML_TYPE);
			javaMailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private String createText(Confirmation confirmation) {
		Map<String, Object> map = new HashMap<>();
		map.put(CONFIRMATION_LINK_ATTRIBUTE, Properties.getConfirmationLink());
		map.put(CONFIRMATION_ATTRIBUTE, confirmation);

		return getStringFromTemplate(map);
	}

	private String getStringFromTemplate(Map<String, Object> map) {
		return mergeTemplateIntoString(velocityEngine, mailTemplate.getVelocityURL(), ENCODING, map);
	}

	private Date calculateExpiresEnd(Date createdDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(createdDate);
		calendar.add(Calendar.HOUR, Properties.getExpiresConfirmationHours());

		return calendar.getTime();
	}

	private String createToken(User user) {
		return new BCryptPasswordEncoder().encode(user.getEmail() + user.getLogin());
	}

	private boolean isValidConfirmationDate(Confirmation confirmation) {
		return confirmation != null && confirmation.getExpiresEnd().after(new Date());
	}
}
