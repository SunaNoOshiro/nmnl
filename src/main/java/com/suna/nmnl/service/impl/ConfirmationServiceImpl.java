package com.suna.nmnl.service.impl;

import javax.annotation.Resource;

import com.suna.nmnl.model.Confirmation;
import com.suna.nmnl.repository.ConfirmationRepository;
import com.suna.nmnl.service.ConfirmationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class ConfirmationServiceImpl implements ConfirmationService {

	@Resource(name = Repositories.CONFIRMATION_REPOSITORY)
	private ConfirmationRepository confirmationRepository;

	@Override
	public Confirmation findOne(String token) {
		return confirmationRepository.findOne(token);
	}

	@Override
	public boolean exist(String token) {
		return confirmationRepository.exists(token);
	}

	@Override
	public void delete(Confirmation confirmation) {
		confirmationRepository.delete(confirmation);
	}

	@Override
	public Confirmation save(Confirmation confirmation) {
		return confirmationRepository.save(confirmation);
	}
}
