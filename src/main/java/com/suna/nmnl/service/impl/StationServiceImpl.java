package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Station;
import com.suna.nmnl.repository.StationRepository;
import com.suna.nmnl.service.StationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class StationServiceImpl implements StationService {

	@Resource(name = Repositories.STATION_REPOSITORY)
	private StationRepository stationRepository;

	@Override
	public Station save(Station station) {
		if (station == null || station.getUser() == null || station.getUser().getId() == null)
			return null;
		return stationRepository.save(station);
	}

	@Override
	public Station find(Long id) {
		return stationRepository.findOne(id);
	}

	@Override
	public Station findWithRates(Long id) {
		return stationRepository.findWithRates(id);
	}

	@Override
	public List<Station> findAll() {
		return stationRepository.findAll();
	}

	@Override
	public List<Station> findAllWithRates() {
		return stationRepository.findAllWithRates();
	}

	@Override
	public void delete(Station station) {
		stationRepository.delete(station);
	}

	@Override
	public void delete(Long stationId) {
		stationRepository.delete(stationId);
	}

	@Override
	public List<Station> findByUserLogin(String login) {
		return stationRepository.findByUserLogin(login);
	}

	@Override
	public boolean exist(Long stationId) {
		return stationRepository.exists(stationId);
	}

	@Override
	public List<Station> findByUserId(Long userId) {
		return stationRepository.findByUserId(userId);
	}

}
