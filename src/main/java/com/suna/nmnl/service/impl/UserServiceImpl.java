package com.suna.nmnl.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.model.Role;
import com.suna.nmnl.model.User;
import com.suna.nmnl.repository.RoleRepository;
import com.suna.nmnl.repository.StationRepository;
import com.suna.nmnl.repository.UserRepository;
import com.suna.nmnl.service.UserService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class UserServiceImpl implements UserService {

	@Resource(name = Repositories.USER_REPOSITORY)
	private UserRepository userRepository;

	@Resource(name = Repositories.ROLE_REPOSITORY)
	private RoleRepository roleRepository;

	@Resource(name = Repositories.STATION_REPOSITORY)
	private StationRepository stationRepository;

	@Override
	public User findOne(Long id) {
		User user = userRepository.findOne(id);
		if (user != null) {
			List<Role> roles = roleRepository.findByUserId(user.getId());
			user.setRoles(roles);
		}
		return user;
	}

	@Override
	public User save(User user) {
		List<Role> roles = new ArrayList<>();
		roles.add(roleRepository.findByName(RoleEnum.ROLE_USER));
		user.setRoles(roles);
		user.setActive(false);
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		return userRepository.save(user);
	}

	@Override
	public User update(User user) {
		return userRepository.save(user);
	}

	@Override
	public void delete(User user) {
		userRepository.delete(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findByLogin(String login) {
		User user = userRepository.findByLogin(login);
		if (user != null) {
			List<Role> roles = roleRepository.findByUserId(user.getId());
			user.setRoles(roles);
		}
		return user;
	}

	@Override
	public User findByEmail(String email) {
		User user = userRepository.findByEmail(email);
		if (user != null) {
			List<Role> roles = roleRepository.findByUserId(user.getId());
			user.setRoles(roles);
		}
		return user;
	}

	@Override
	public User findByToken(String token) {
		return userRepository.findByToken(token);
	}

	@Override
	public boolean exist(Long userId) {
		return userRepository.exists(userId);
	}

	@Override
	public User findOneWithStations(Long id) {
		User user = findOne(id);
		user.setStations(stationRepository.findByUserId(id));
		return user;
	}

}
