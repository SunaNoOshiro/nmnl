package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Song;
import com.suna.nmnl.repository.PlayingTypeRepository;
import com.suna.nmnl.service.RecommendationService;
import com.suna.nmnl.service.SongService;
import com.suna.nmnl.service.StationService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;
import com.suna.nmnl.util.ConstantUtil.Beans.Services;

public class RecommendationServiceImpl implements RecommendationService {

	@Resource(name = Services.SONG_SERVICE)
	private SongService songService;

	@Resource(name = Services.STATION_SERVICE)
	private StationService stationService;

	@Resource(name = Repositories.PLAYING_TYPE_REPOSITORY)
	private PlayingTypeRepository playingTypeRepository;

	@Override
	public List<Song> getRecommendationSongs(Long StationId) {
		return songService.findSongsByStation(StationId);
	}

	@Override
	public Song getRecommendationSong(Long StationId) {
		List<Song> songs = songService.findSongsByStation(StationId);
		for (Song song : songs) {
			if (!songService.playedToday(song.getId())) {
				// Station station = stationService.findWithRates(1L);
				// PlayingType type = playingTypeRepository.findOne(1L);
				// station.getLastPlayed().put(song, type );
				// stationService.save(station);
				return song;
			}
		}

		return null;
	}

}
