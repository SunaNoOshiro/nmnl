package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Album;
import com.suna.nmnl.repository.AlbumRepository;
import com.suna.nmnl.service.AlbumService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class AlbumServiceImpl implements AlbumService {

	@Resource(name = Repositories.ALBUM_REPOSITORY)
	private AlbumRepository albumRepository;

	@Override
	public List<Album> findAlbumsByQuery(String query) {
		return albumRepository.findAlbumsByQuery(query == null ? "" : query.toLowerCase());
	}

	@Override
	public Album findOne(Long id) {
		return albumRepository.findOne(id);
	}

	@Override
	public List<Album> findAlbumsByArtistId(Long id) {
		return albumRepository.findAlbumsByArtistId(id);
	}

}
