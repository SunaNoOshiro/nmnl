package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Artist;
import com.suna.nmnl.repository.ArtistRepository;
import com.suna.nmnl.service.ArtistService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class ArtistServiceImpl implements ArtistService {

	@Resource(name = Repositories.ARTIST_REPOSITORY)
	private ArtistRepository artistRepository;

	@Override
	public List<Artist> findArtistsByQuery(String query) {
		return artistRepository.findArtistsByQuery(query == null ? "" : query.toLowerCase());
	}

	@Override
	public Artist findOne(Long id) {
		return artistRepository.findOne(id);
	}

}
