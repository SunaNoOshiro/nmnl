package com.suna.nmnl.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.suna.nmnl.model.Song;
import com.suna.nmnl.repository.GenreRepository;
import com.suna.nmnl.repository.SongRepository;
import com.suna.nmnl.service.SongService;
import com.suna.nmnl.util.ConstantUtil.Beans.Repositories;

public class SongServiceImpl implements SongService {

	@Resource(name = Repositories.SONG_REPOSITORY)
	private SongRepository songRepository;

	@Resource(name = Repositories.GENRE_REPOSITORY)
	private GenreRepository genreRepository;

	@Override
	public Song findOne(Long id) {
		return songRepository.findOne(id);
	}

	@Override
	public Song findSongWithGenres(Long id) {
		Song song = songRepository.findOne(id);
		song.setGenres(genreRepository.findAll());
		return song;
	}

	@Override
	public Song save(Song song) {
		return songRepository.save(song);
	}

	@Override
	public void delete(Song song) {
		songRepository.delete(song);
	}

	@Override
	public List<Song> findAll() {
		return songRepository.findAll();
	}

	@Override
	public List<Song> findSongsByQuery(String query) {
		return songRepository.findSongsByQuery(query == null ? "" : query.toLowerCase());
	}

	@Override
	public List<Song> findSongsByStation(Long id) {
		return songRepository.findSongsByStation(id);
	}

	@Override
	public boolean playedToday(Long id) {
		return songRepository.playedToday(id);
	}

	@Override
	public Song fetch(Long id) {
		return songRepository.fetch(id);
	}

	@Override
	public List<Song> findSongsByAlbum(Long id) {
		return songRepository.findSongsByStation(id);
	}

	@Override
	public List<Song> findSongsByGenre(Long id) {
		return songRepository.findSongsByGenre(id);
	}

}
