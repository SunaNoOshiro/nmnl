package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.Album;

public interface AlbumService {

	List<Album> findAlbumsByQuery(String query);

	List<Album> findAlbumsByArtistId(Long id);

	Album findOne(Long id);

}
