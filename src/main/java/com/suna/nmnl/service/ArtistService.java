package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.Artist;

public interface ArtistService {
	List<Artist> findArtistsByQuery(String query);

	Artist findOne(Long id);
}
