package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.Song;

public interface RecommendationService {

	List<Song> getRecommendationSongs(Long stationId);

	Song getRecommendationSong(Long stationId);
}
