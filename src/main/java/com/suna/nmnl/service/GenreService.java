package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.Genre;

public interface GenreService {

	List<Genre> findGenreByQuery(String query);

	Genre findOne(Long id);
}
