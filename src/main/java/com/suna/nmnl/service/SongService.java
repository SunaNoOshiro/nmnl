package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.Song;

public interface SongService {
	
	Song findOne(Long id);

	Song save(Song song);

	void delete(Song song);

	Song findSongWithGenres(Long id);

	List<Song> findAll();

	List<Song> findSongsByQuery(String query);

	List<Song> findSongsByStation(Long id);
	
	List<Song> findSongsByAlbum(Long id);
	
	List<Song> findSongsByGenre(Long id);

	boolean playedToday(Long id);

	Song fetch(Long id);
}
