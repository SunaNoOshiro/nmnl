package com.suna.nmnl.service;

import com.suna.nmnl.model.Confirmation;

public interface ConfirmationService {

	Confirmation save(Confirmation confirmation);

	Confirmation findOne(String token);

	boolean exist(String token);

	void delete(Confirmation confirmation);

}
