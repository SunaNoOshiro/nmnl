package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.User;

public interface UserService {
	User findOne(Long id);
	User findOneWithStations(Long id);
	User save(User user);
	void delete(User user);
	List<User> findAll();
	User findByLogin(String login);
	User findByEmail(String email);
	User findByToken(String token);
	User update(User user);
	boolean exist(Long userId);
}
