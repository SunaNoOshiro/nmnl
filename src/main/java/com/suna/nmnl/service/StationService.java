package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.model.Station;

public interface StationService {

	Station save(Station station);

	Station find(Long id);

	List<Station> findAll();

	void delete(Station station);

	boolean exist(Long stationId);

	List<Station> findByUserLogin(String login);

	List<Station> findByUserId(Long userId);

	void delete(Long stationId);

	List<Station> findAllWithRates();

	Station findWithRates(Long id);

}
