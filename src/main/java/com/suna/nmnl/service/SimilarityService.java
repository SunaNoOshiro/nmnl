package com.suna.nmnl.service;

import java.util.List;

public interface SimilarityService<T> {
	List<T> findSimilar(T object);
}
