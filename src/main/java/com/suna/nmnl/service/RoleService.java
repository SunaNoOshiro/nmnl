package com.suna.nmnl.service;

import java.util.List;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.model.Role;

public interface RoleService {
	Role findOne(Long id);
	Role save(Role role);
	void delete(Role role);
	List<Role> findAll();
	Role findByName(RoleEnum name);
}
