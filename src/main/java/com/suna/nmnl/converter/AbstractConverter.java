package com.suna.nmnl.converter;

import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.suna.nmnl.populator.AbstractPopulator;

public class AbstractConverter<SOURCE, TARGET> implements Converter<SOURCE, TARGET> {

	private List<AbstractPopulator<SOURCE, TARGET>> populators;

	private Class<SOURCE> sourceClass;
	private Class<TARGET> targetClass;

	@Override
	public TARGET convert(SOURCE source) {
		if (sourceClass == null || targetClass == null || source == null)
			return null;

		TARGET target = createTargetObject();

		for (AbstractPopulator<SOURCE, TARGET> populator : getPopulators()) {
			populator.populate(source, target);
		}

		return target;
	}

	private TARGET createTargetObject() {
		try {
			 return  targetClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private List<AbstractPopulator<SOURCE, TARGET>> getPopulators() {
		return populators;
	}

	public void setPopulators(List<AbstractPopulator<SOURCE, TARGET>> populators) {
		this.populators = populators;
	}

	public void setTargetClass(Class<TARGET> targetClass) {
		this.targetClass = targetClass;
	}

	public void setSourceClass(Class<SOURCE> sourceClass) {
		this.sourceClass = sourceClass;
	}
}
