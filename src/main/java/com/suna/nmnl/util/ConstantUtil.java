package com.suna.nmnl.util;

import org.springframework.beans.factory.annotation.Value;

public class ConstantUtil {

    public interface Web {

        String URL_SEPARATOR = "/";
        String CLOSE_VARIABLE_SYMBOL = "}";
        String OPEN_VARIABLE_SYMBOL = "{";

        String RECOMMENDATIONS = "recommendations";
        String SONGS = "songs";
        String SEARCH = "search";
        String ALBUMS = "albums";
        String ARTISTS = "artists";
        String GENRES = "genres";
        String STATIONS = "stations";
        String USERS = "users";
        String LOGIN = "login";
        String AVAILABLE = "available";
        String EMAIL = "email";
        String PLAY = "play";
        String CONFIRMATION = "confirmation";
        String MY = "my";
        String STATION = "station";

        String JSON_CONTENT_TYPE = "Content-type=application/json";

        interface Page {

            String REDIRECT_PREFIX = "redirect:";
            String REDIRECT_TO = REDIRECT_PREFIX + URL_SEPARATOR;

            String HOME_PAGE = "home";
            String LOGIN_PAGE = "login";
            String REGISTRATION_PAGE = "registration";

            String REDIRECT_TO_LOGIN_PAGE = REDIRECT_TO + LOGIN_PAGE;
        }

        interface PathVariables {

            String QUERY = "query";
            String ID = "id";
        }

        interface ModelAttributes {

            String SONG_MODEL = "song";
            String USER_MODEL = "user";
            String MESSAGE = "message";
            String REGISTRATION_STATUS = "registration";
        }

        interface RequestParams {

            String EMAIL = "email";
            String LOGIN = "login";
            String TOKEN = "token";
        }

        interface RequestMappingPath {

            String BASE_API_URL = "/api";

            String SONGS_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + SONGS;
            String RECOMENDATIONS_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + RECOMMENDATIONS;
            String ALBUMS_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + ALBUMS;
            String ARTISTS_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + ARTISTS;
            String GENRES_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + GENRES;
            String STATIONS_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + STATIONS;
            String USERS_API_BASE_URL = BASE_API_URL + URL_SEPARATOR + USERS;
            String REGISTRATION_BASE_URL = URL_SEPARATOR + Page.REGISTRATION_PAGE;

            interface UrlPart {

                String FIND_BY_ID = URL_SEPARATOR + OPEN_VARIABLE_SYMBOL + PathVariables.ID + CLOSE_VARIABLE_SYMBOL;
                String FIND_BY_QUERY = URL_SEPARATOR + SEARCH + URL_SEPARATOR + OPEN_VARIABLE_SYMBOL + PathVariables.QUERY + CLOSE_VARIABLE_SYMBOL;
                String RECOMENDATION_FOR_STATION = URL_SEPARATOR + STATION + URL_SEPARATOR + OPEN_VARIABLE_SYMBOL + PathVariables.ID + CLOSE_VARIABLE_SYMBOL;
                String MY_STATIONS_BY_ID = URL_SEPARATOR + MY + URL_SEPARATOR + PathVariables.ID + CLOSE_VARIABLE_SYMBOL;
                String MY_STATIONS = URL_SEPARATOR + MY;
                String FIND_BY_USERS_ID = URL_SEPARATOR + USERS + OPEN_VARIABLE_SYMBOL + PathVariables.ID + CLOSE_VARIABLE_SYMBOL;
                String HOME = URL_SEPARATOR + PLAY;
                String LOGIN_PAGE = URL_SEPARATOR + LOGIN;

                String CONFIRM = URL_SEPARATOR + CONFIRMATION;
                String AVAILABLE_LOGIN = URL_SEPARATOR + AVAILABLE + URL_SEPARATOR + LOGIN;
                String AVAILABLE_EMAIL = URL_SEPARATOR + AVAILABLE + URL_SEPARATOR + EMAIL;

            }
        }
    }

    public interface Beans {

        interface Services {

            String ALBUM_SERVICE = "albumService";
            String ARTIST_SERVICE = "artistService";
            String CLUSTERING_SERVICE = "clusteringService";
            String CONFIRMATION_SERVICE = "confirmationService";
            String GENRE_SERVICE = "genreService";
            String RECOMMENDATION_SERVICE = "recommendationService";
            String ROLE_SERVICE = "roleService";
            String SIMILARITY_SERVICE = "similarityService";
            String SONG_SERVICE = "songService";
            String STATION_SERVICE = "stationService";
            String USER_SERVICE = "userService";
            String MAIL_CONFIRMATION_SERVICE = "mailConfirmationService";
        }

        interface Facades {

            String ALBUM_FACADE = "albumFacade";
            String ARTIST_FACADE = "artistFacade";
            String CLUSTERING_FACADE = "clusteringFacade";
            String CONFIRMATION_FACADE = "confirmationFacade";
            String GENRE_FACADE = "genreFacade";
            String RECOMMENDATION_FACADE = "recommendationFacade";
            String ROLE_FACADE = "roleFacade";
            String SIMILARITY_FACADE = "similarityFacade";
            String SONG_FACADE = "songFacade";
            String STATION_FACADE = "stationFacade";
            String USER_FACADE = "userFacade";
            String MAIL_CONFIRMATION_FACADE = "mailConfirmationFacade";
        }

        interface Repositories {

            String ALBUM_REPOSITORY = "albumRepository";
            String ARTIST_REPOSITORY = "artistRepository";
            String CONFIRMATION_REPOSITORY = "confirmationRepository";
            String GENRE_REPOSITORY = "genreRepository";
            String PLAYING_TYPE_REPOSITORY = "playingTypeRepository";
            String RATE_REPOSITORY = "rateRepository";
            String ROLE_REPOSITORY = "roleRepository";
            String SONG_REPOSITORY = "songRepository";
            String STATION_REPOSITORY = "stationRepository";
            String USER_REPOSITORY = "userRepository";
        }

        interface Converters {

            String USER_MODEL_CONVERTER = "userModelConverter";
            String USER_DTO_CONVERTER = "userDTOConverter";
            String STATION_MODEL_CONVERTER = "stationModelConverter";
            String STATION_DTO_CONVERTER = "stationDTOConverter";
            String SONG_MODEL_CONVERTER = "songModelConverter";
            String SONG_DTO_CONVERTER = "songDTOConverter";
            String RATE_MODEL_CONVERTER = "rateModelConverter";
            String RATE_DTO_CONVERTER = "rateDTOConverter";
            String PLAYING_TYPE_MODEL_CONVERTER = "playingTypeModelConverter";
            String PLAYING_TYPE_DTO_CONVERTER = "playingTypeDTOConverter";
            String GENRE_MODEL_CONVERTER = "genreModelConverter";
            String GENRE_DTO_CONVERTER = "genreDTOConverter";
            String ARTIST_MODEL_CONVERTER = "artistModelConverter";
            String ARTIST_DTO_CONVERTER = "artistDTOConverter";
            String ALBUM_MODEL_CONVERTER = "albumModelConverter";
            String ALBUM_DTO_CONVERTER = "albumDTOConverter";
        }

        interface MailTemplates {

            String MAIL_SENDER = "mailSender";
            String VELOСITY_ENGINE = "velocityEngine";
            String CONFIRMATION_MAIL_TEMPLATE = "confirmationMailTemplate";
        }
    }

    public static class Properties {

        @Value("${mailtemplate.confirmation.url}")
        private static String confirmationLink;

        @Value("${confirmation.expires.hours}")
        private static int expiresConfirmationHours;

        public static String getConfirmationLink() {
            return confirmationLink;
        }

        public static int getExpiresConfirmationHours() {
            return expiresConfirmationHours;
        }
    }

    public interface Message {

        interface RegistrationMessage {

            String REGISTRATION_COMPLETED_MESSAGE = "registration.completed";
            String REGISTRATION_UNCOMPLETED_MESSAGE = "registration.uncompleted";
            String REGISTRATION_CHECK_MESSAGE = "registration.check";
        }
    }

}
