package com.suna.nmnl.json.serialize;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.suna.nmnl.model.AbstractModel;

public class GenericKeySerializer<T extends AbstractModel> extends JsonSerializer<T> {
	private static final String SEPARATOR = "_";

	@Override
	public void serialize(T value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException {
		gen.writeFieldName(value.getClass().getSimpleName().toLowerCase() + SEPARATOR + value.getId());

	}

}
