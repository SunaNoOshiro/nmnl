package com.suna.nmnl.json.deserialize;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.suna.nmnl.service.SongService;

@Component
public class SongKeyDeserializer extends KeyDeserializer {
	private static SongService songService;

	// Required by Jackson annotation to instantiate the serializer
	public SongKeyDeserializer() {
	}

	@Autowired
	public SongKeyDeserializer(SongService designService) {
		SongKeyDeserializer.songService = designService;
	}

	@Override
	public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException {

		if (!key.matches("^song_\\d*$"))
			return null;

		String idString = key.replace("song_", "");
		try {
			return songService.findOne(Long.parseLong(idString));
		} catch (NumberFormatException e) {
			return null;
		}
	}

}
