package com.suna.nmnl.json.deserialize;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.suna.nmnl.service.GenreService;

@Component
public class GenreKeyDeserializer extends KeyDeserializer {
	private static GenreService service;

	// Required by Jackson annotation to instantiate the serializer
	public GenreKeyDeserializer() {
	}

	@Autowired
	public GenreKeyDeserializer(GenreService designService) {
		GenreKeyDeserializer.service = designService;
	}

	@Override
	public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException {

		if (!key.matches("^genre_\\d*$"))
			return null;

		String idString = key.replace("genre_", "");
		try {
			return service.findOne(Long.parseLong(idString));
		} catch (NumberFormatException e) {
			return null;
		}
	}

}
