package com.suna.nmnl.json.deserialize;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.suna.nmnl.service.AlbumService;

@Component
public class AlbumKeyDeserializer extends KeyDeserializer {
	private static AlbumService service;

	// Required by Jackson annotation to instantiate the serializer
	public AlbumKeyDeserializer() {
	}

	@Autowired
	public AlbumKeyDeserializer(AlbumService designService) {
		AlbumKeyDeserializer.service = designService;
	}

	@Override
	public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException {

		if (!key.matches("^album_\\d*$"))
			return null;

		String idString = key.replace("album_", "");
		try {
			return service.findOne(Long.parseLong(idString));
		} catch (NumberFormatException e) {
			return null;
		}
	}

}
