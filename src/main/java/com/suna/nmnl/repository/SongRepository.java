package com.suna.nmnl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.suna.nmnl.model.Song;

@Repository
public interface SongRepository extends JpaRepository<Song, Long> {

	Song findBySoundcloudId(Long value);

	@Query(value = "Select * From song Where Lower(title) Like %:query% Limit 5", nativeQuery = true)
	List<Song> findSongsByQuery(@Param("query") String query);

	@Query(value = "Select * From song Where id In (Select song_id From songs_rate Where station_id = :id)", nativeQuery = true)
	List<Song> findSongsByStation(@Param("id") Long id);

	@Query(value = "Select * From song Where album_id = :id)", nativeQuery = true)
	List<Song> findSongsByAlbum(@Param("id") Long id);

	@Query(value = "SELECT DISTINCT song FROM Song song LEFT JOIN FETCH song.artist LEFT JOIN FETCH song.album Where song.id = :id")
	Song fetch(@Param("id") Long id);

	@Query(value = "SELECT CASE WHEN EXISTS ( SELECT 1   FROM last_played  WHERE song_id = :id AND play_type_id = 1) THEN 'true' ELSE 'false' END ", nativeQuery = true)
	boolean playedToday(@Param("id") Long id);

	@Query(value = "Select * From song Where id In (Select song_id from song_genre Where genres_id = :id)", nativeQuery = true)
	List<Song> findSongsByGenre(@Param("id")Long id);

}
