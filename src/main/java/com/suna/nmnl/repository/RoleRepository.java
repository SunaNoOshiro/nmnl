package com.suna.nmnl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.suna.nmnl.enums.RoleEnum;
import com.suna.nmnl.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByName(RoleEnum name);

	@Query(value = "Select * From role Where id In (Select roles_id From users_role Where users_id = :user )", nativeQuery = true)
	List<Role> findByUserId(@Param("user") Long userId);

}
