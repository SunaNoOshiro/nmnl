package com.suna.nmnl.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.nmnl.model.Rate;

public interface RateRepository extends JpaRepository<Rate, Long>{

}
