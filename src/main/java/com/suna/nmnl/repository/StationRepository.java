package com.suna.nmnl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.suna.nmnl.model.Station;

public interface StationRepository extends JpaRepository<Station, Long> {
	@Query(value = "Select * From station Where user_id = (Select id From users Where login = :login )", nativeQuery = true)
	List<Station> findByUserLogin(@Param("login") String login);

	@Query(value = "Select * From station Where user_id = :user ", nativeQuery = true)
	List<Station> findByUserId(@Param("user") Long userId);

	@Query(value = "SELECT DISTINCT stat FROM Station stat LEFT JOIN FETCH stat.songRates")
	List<Station> findAllWithRates();

	@Query(value = "SELECT DISTINCT stat FROM Station stat LEFT JOIN FETCH stat.songRates LEFT JOIN FETCH stat.lastPlayed Where stat.id = :id")
	Station findWithRates(@Param("id") Long id);

}
