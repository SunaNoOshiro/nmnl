package com.suna.nmnl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.suna.nmnl.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String value);

	User findByLogin(String value);

	@Query(value = "SELECT * from users WHERE id = (SELECT user_id FROM confirmation WHERE token = :token)", nativeQuery = true)
	User findByToken(@Param("token") String token);
}
