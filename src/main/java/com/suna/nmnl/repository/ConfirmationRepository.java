package com.suna.nmnl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.suna.nmnl.model.Confirmation;

@Repository
public interface ConfirmationRepository extends JpaRepository<Confirmation, String> {
}
