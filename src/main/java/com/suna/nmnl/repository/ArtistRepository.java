package com.suna.nmnl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.suna.nmnl.model.Artist;

public interface ArtistRepository extends JpaRepository<Artist, Long> {

	@Query(value = "Select * from Artist Where Lower(name) Like %:query% Limit 5", nativeQuery = true)
	List<Artist> findArtistsByQuery(@Param("query") String query);

}
