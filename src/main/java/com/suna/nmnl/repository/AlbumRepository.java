package com.suna.nmnl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.suna.nmnl.model.Album;

public interface AlbumRepository extends JpaRepository<Album, Long> {

	@Query(value = "Select * From album Where Lower(name) Like %:query% Limit 5", nativeQuery = true)
	List<Album> findAlbumsByQuery(@Param("query") String query);
	
	@Query(value = "Select * From album Where artist_id = :id ", nativeQuery = true)
	List<Album> findAlbumsByArtistId(@Param("id") Long id);

}
