package com.suna.nmnl.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.nmnl.model.PlayingType;

public interface PlayingTypeRepository extends JpaRepository<PlayingType, Long>{}