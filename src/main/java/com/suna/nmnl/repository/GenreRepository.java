package com.suna.nmnl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.suna.nmnl.model.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {

	@Query(value = "Select * From genre Where Lower(name) Like %:query%", nativeQuery = true)
	List<Genre> findGenreByQuery(@Param("query") String query);
}
